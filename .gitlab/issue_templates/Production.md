[//]: # (Please title your issue: "Production for tag X.Y.Z-A-B")

# Tag X.Y.Z-A-B

Code is [here](https://gitlab.cern.ch/htx/MultibjetsAnalysis/tags/MultibjetsAnalysis-XXX)

## Questions About Production

Send an email to Loic <lvalery@cern.ch> or to the mailing list <atlas-phys-exotics-HtX-4tops@cern.ch>

## Related Issues:

- #AA

## Job Details

The jobs are ran with the following configurations:

:x: with systematics

:x: with output trees

:x: with data

## Instructions

From your top level directory, do:

```python MultibjetsAnalysis/scripts/submit.py sample1.list sample2.list```

Be sure to provide Loïc the links to your panda pages for tracking.

## Responsibilities

[//]: # (When something is launched, please add :white_check_mark: )

| Launched/Sample Group             | Username | BigPanda Page | Issues |
|:----------------------------------|:---------|:--------------|:-------|
| :x: data                          |          | [link]()      |        |
| :x: QCD                           |          | [link]()      |        |
| :x: VLQ                           |          | [link]()      |        |
| :x: Hplus                         |          | [link]()      |        |
| :x: HBSM                          |          | [link]()      |        |
| :x: FCNC                          |          | [link]()      |        |
| :x: UEDRPP                        |          | [link]()      |        |
| :x: diboson                       |          | [link]()      |        |
| :x: singletop                     |          | [link]()      |        |
| :x: topEW                         |          | [link]()      |        |
| :x: ttbar                         |          | [link]()      |        |
| :x: W_sherpa                      |          | [link]()      |        |
| :x: Z_sherpa                      |          | [link]()      |        |

## Progress

- [ ] Launch production
- [ ] Production finished
- [ ] Start transfer of output to storage
- [ ] Transfer finished
- [ ] Document on twiki

## Making Changes

Please edit the issue to make changes to any checkboxes and so forth.