#ifndef TOPTTBARPTREWEIGHTING_H
#define TOPTTBARPTREWEIGHTING_H

#include <string>
#include <map>
#include "TH1F.h"

class NNLOReweighter;
class TopTtbarPtReweighting {
    
public:
    
    struct TopTtbarPtReweightingResult {
        TopTtbarPtReweightingResult() : nominal(1.) {}
        double nominal;
        std::map < std::string, double > systematics;
    };
    
    //
    // Standard c++
    //
    TopTtbarPtReweighting( const int mc_chnnel_number, const bool useSysts = false );
    TopTtbarPtReweighting( const TopTtbarPtReweighting & );
    ~TopTtbarPtReweighting();
    
    //
    // Specific functions
    //
    bool Clear();
    bool FindTops( const xAOD::TruthParticleContainer * );
    TopTtbarPtReweightingResult GetEventWeight() const ;
    void GetTopTtbarPt( double &topPt, double &ttbarPt ) const ;
    bool IsSelfDecayed( const xAOD::TruthParticle *part ) const ;

    float GetTop1LWeight(const double ttbarPt) const;
    
private:    
    const xAOD::TruthParticle *m_top;
    const xAOD::TruthParticle *m_antitop;
    bool m_useSyst;
    std::vector < std::string > *m_systs;
    NNLOReweighter* m_ttbarNNLO_rw_tool;
    TH1F* m_1l_reweight;
};

#endif //TOPTTBARPTREWEIGHTING_H
