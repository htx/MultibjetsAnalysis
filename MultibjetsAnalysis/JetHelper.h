#ifndef MultibjetsAnalysis_JetHelper_h
#define MultibjetsAnalysis_JetHelper_h

#include <utility>

// xAOD
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODRootAccess/TStore.h"

#include "MultibjetsAnalysis/HelperFunctions.h"

#include "JetSubStructureUtils/Nsubjettiness.h"
#include "JetSubStructureUtils/KtSplittingScale.h"

#include <memory>

// jet reclustering
#include <fastjet/JetDefinition.hh>

// top tagger
#include "BoostedJetTaggers/SubstructureTopTagger.h"

namespace ST{
class SUSYObjDef_xAOD;
}

class JetHelper
{

 public:

  JetHelper(ST::SUSYObjDef_xAOD*& susy_tools, xAOD::TEvent*& event, xAOD::TStore*& store);
  virtual ~JetHelper();

  // Apply kinematic and quality cuts
  void DecorateBaselineJets(xAOD::JetContainer*& jets, std::string sys_name = "");

  // Retreive the fat jet container
  void RetrieveFatJets(const std::string& fj_key,
        SubstructureTopTagger* top_tagger_smooth_tight = 0,
        SubstructureTopTagger* top_tagger_smooth_loose = 0,
        SubstructureTopTagger* top_tagger_lowpt_tight = 0,
        SubstructureTopTagger* top_tagger_lowpt_loose = 0,
        SubstructureTopTagger* top_tagger_highpt_tight = 0,
        SubstructureTopTagger* top_tagger_highpt_loose = 0);

  // Retreive the cluster container
  void RetrieveClusters(const std::string& clus_key);

  // Retrieve the fat jets
  void RetrieveSignalFatJets();
  //void RetrieveFatJetsST(xAOD::JetContainer*& jets, xAOD::ShallowAuxContainer*& jets_aux);

  xAOD::CaloClusterContainer*    GetClustersShallowCopy() {return m_clus_copy;};

  UInt_t GetNbBadJets(){ return m_nb_bad_jets; };

  const xAOD::JetContainer* GetJets(bool is_nominal, bool affects_jets, const std::string &sys_name, std::string jet_key){
     const xAOD::JetContainer* jets(nullptr);
     if(!is_nominal && affects_jets) jet_key += sys_name;
     if(!m_store->retrieve( jets, jet_key ).isSuccess()){
       std::cout << "JetHelper::GetJets() major problem retrieving " << jet_key << std::endl;
     }
     return jets;
  };

  const xAOD::JetContainer* GetBaselineJets(bool is_nominal, bool affects_jets, const std::string &sys_name)   {
    return GetJets(is_nominal, affects_jets, sys_name, "BaselineJets");
  };
  const xAOD::JetContainer* GetSignalJets(bool is_nominal, bool affects_jets, const std::string &sys_name) {
    return GetJets(is_nominal, affects_jets, sys_name, "SignalJets");
  };
  const xAOD::JetContainer* GetSignalBTaggableJets(bool is_nominal, bool affects_jets, const std::string &sys_name)   {
    return GetJets(is_nominal, affects_jets, sys_name, "SignalBJets");
  };

  const xAOD::JetContainer* GetSignalFatJets(){return m_fat_jets;};

 protected:

   xAOD::TEvent*& m_event;  //!
   xAOD::TStore*& m_store;  //!

  // SUSYTools
  ST::SUSYObjDef_xAOD*& m_susy_tools ; //!

  // shallow copies
  xAOD::JetContainer*        m_jets_copy; //!
  xAOD::ShallowAuxContainer* m_jets_copy_aux; //!

  xAOD::JetContainer*        m_fj_copy; //!
  xAOD::ShallowAuxContainer* m_fj_copy_aux; //!

  xAOD::CaloClusterContainer*        m_clus_copy; //!
  xAOD::ShallowAuxContainer* m_clus_copy_aux; //!

  // the jet list for our standard fat jets as well
  const xAOD::JetContainer* m_fat_jets;

  // number of bad jets
  UInt_t m_nb_bad_jets;

  fastjet::ClusterSequence* m_cur_cs;

  double m_smallR_eta_cut;

};

#endif
