#ifndef MultibjetsAnalysis_MainAnalysis_H
#define MultibjetsAnalysis_MainAnalysis_H

#include <EventLoop/Algorithm.h>

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include <TH1.h>
#include <TTree.h>

#include "PATInterfaces/SystematicVariation.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/SystematicCode.h"

#include <TLorentzVector.h>

//Metadata
#include "xAODMetaDataCnv/FileMetaDataTool.h"
#include "xAODTriggerCnv/TriggerMenuMetaDataTool.h"

// top tagger
#include "BoostedJetTaggers/SubstructureTopTagger.h"

// jet reclustering
#include <AsgTools/AnaToolHandle.h>
#include <JetInterface/IJetExecuteTool.h>

// SUSYTools
#include "SUSYTools/SUSYObjDef_xAOD.h"

// Standard library
#include <map>
#include <string>

// GRL
class GoodRunsListSelectionTool;

/*namespace ST{
  class SUSYObjDef_xAOD;
}*/

class TreeMaker;
class TopTtbarPtReweighting;
class ttbbNLO_syst;
class TruthTaggingManager;

enum weight {
  mc,
  pileup,
  btaggingSF,
  elecSF,
  elecTrigSF,
  muonSF,
  muonTrigSF,
  jvtSF
};

typedef asg::AnaToolHandle<IJetExecuteTool> JRT_t;

class MainAnalysis : public EL::Algorithm
{

  public:
    // Specify the data source [0 = Data, 1 = FullSim, 2 = FastSim, default = undef]
    int    m_dataSource;
    // enable to make reclustered jets and use as large-R jets
    bool   m_doVRcJets;
    bool   m_doRcJets;
    int	   m_maxjets_mjsum;
    // JMS on small-R jets which is propagated into MJSum calculation
    float m_akt4_JMS;
    // enable to use large-R jets instead
    bool   m_doAK10Jets;
    // enable to do pileup reweighting
    bool   m_doPileup;
    // enable to process systematics
    bool   m_doSyst;
    // enable to process truth
    bool   m_doTruth;
    // enable to create verbose histfitter output
    bool   m_doVerboseOutput;
    // enable to do top pt correction
    bool   m_doTopPtCorrection;
    // Computes the ttbar+HF classification [0: no, 1: classifiction only, 2:nominal correction, 3: systematics]
    int   m_doTtbarHFClassification;
    // enable to produce an ntuple
    bool   m_doNTUP;
    // enable to produce systematics in the ntuples
    bool   m_doNTUPSyst;
    // enable to do TRF weighting in the TreeMaker only
    bool   m_doTRF;
    // enable to apply the baseline selection
    bool   m_doBaselineSelection;
    // enable to fill in btag efficiencies in the ntuple
    bool   m_dobTagEff;
    // enable to fill in the matrix method in the ntuple
    bool   m_doMM;
    // enable to produce a mini-xAOD output
    bool   m_doxAOD;
    // enable to turn on debug
    bool   m_debug;
    // enable to turn on verbosity
    bool   m_verbose;
    // enable the computation of W/Z+jets rewighting for Sherpa 2.2
    bool   m_doWZReweighting;
    // enable the use of the event weights
    bool   m_useCorrectedWeights;
    // do QCD estimate (use baseline leptons for the selection)
    bool   m_doQCD;
    // config file name
    std::string m_config;
    // JMS uncertainties configuration
    std::string m_JMSunc;

  private:
    // storegate pointers
    xAOD::TEvent* m_event; //!
    xAOD::TStore *m_store;  //!

    // check if file is from a DxAOD
    bool   m_isDerived; //!

    // count number of events looked at
    int m_event_count; //!

    // https://svnweb.cern.ch/trac/atlasoff/browser/Event/xAOD/xAODEventInfo/trunk/Root/EventInfo_v1.cxx#L61
    // store information about the event, run, and physics process we look at
    unsigned long long m_event_number;   //!
    unsigned long m_run_number;     //!
    unsigned long m_random_run_number;     //!
    int m_channel_number; //!

    // whether or not we are running over MC or not
    bool m_isMC; //!

    // trigger map
    std::map < std::string, bool > m_trigger_map;

    // hold all the jet reclustering tools
    std::map<std::string, JRT_t> recluster_vrc_r10rho350_map; //!
    std::map<std::string, JRT_t> recluster_vrc_r10rho300_map; //!
    std::map<std::string, JRT_t> recluster_vrc_r12rho300_map; //!
    std::map<std::string, JRT_t> recluster_vrc_r10rho250_map; //!
    std::map<std::string, JRT_t> recluster_map; //!

    // number of events in the initial xAOD
    uint64_t m_nb_events;  //!
    double m_nb_events_weight; //!
    double m_nb_events_weight_squared; //!

    SubstructureTopTagger* top_tagger_smooth_tight; //!
    SubstructureTopTagger* top_tagger_smooth_loose; //!
    SubstructureTopTagger* top_tagger_lowpt_tight; //!
    SubstructureTopTagger* top_tagger_lowpt_loose; //!
    SubstructureTopTagger* top_tagger_highpt_tight; //!
    SubstructureTopTagger* top_tagger_highpt_loose; //!

#ifndef __CINT__
    GoodRunsListSelectionTool          *m_grl;                //!
    ST::SUSYObjDef_xAOD                *m_susy_tools;         //!
    TreeMaker                          *m_tree_maker;         //!
    xAODMaker::FileMetaDataTool        *m_metadata_tool;      //!
    xAODMaker::TriggerMenuMetaDataTool *m_trigger_metadata_tool; //!
    TopTtbarPtReweighting              *m_topttbarPtrw;       //!
    ttbbNLO_syst                       *m_tool_HFsyst; //!

#endif // not __CINT__

    // store the various enum weights
    double m_weight[8]; //!

    // various other information from metadata
    float m_cut_flow[50]; //!
    float m_cross_section; //!
    float m_rel_uncertainty; //!

    // no idea what this one is, but it's from SUSY::finalState(id1, id2)
    float m_process; //!

    // hold a vector of systematics to look at
    std::vector<ST::SystInfo> m_syst_info_list; //!

    // hold a vector of systematic names used
    std::vector<std::string> m_syst_names; //!

    TruthTaggingManager * m_ttm; //!
    std::vector<std::string> m_btagging_systematics; //!

 public:

  MainAnalysis ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(MainAnalysis, 1);

  // member variables

};

#endif
