#include "MultibjetsAnalysis/BaselineSelection.h"

bool BaselineSelection::keep(
                bool doBaselineSelection,
                const xAOD::MissingETContainer* v_met,
				        const xAOD::JetContainer* v_jets,
				        const xAOD::MuonContainer* v_muons,
				        const xAOD::ElectronContainer* v_electrons,
                const bool doQCD,
				        int flag){

  bool result = false;

  xAOD::MissingETContainer::const_iterator met_it = v_met->find("Final");
  if (met_it == v_met->end()) std::cout << "No RefFinal inside MET container" << std::endl;
  xAOD::MissingET* met = *met_it;

  int muons_n = 0;
  for (auto muon : *v_muons) {
    if(muon->auxdata<char>("baseline")==0) continue;
    if(muon->auxdata<char>("passOR")==0) continue;
    if(!doQCD && muon->auxdata<char>("signal")==0) continue;
    muons_n ++;
  }

  int electrons_n = 0;
  for (auto electron : *v_electrons) {
    if(electron->auxdata<char>("baseline")==0) continue;
    if(electron->auxdata<char>("passOR")==0) continue;
    if(!doQCD && electron->auxdata<char>("signal")==0) continue;
    electrons_n ++;
  }

  int nb_jets_signal = 0;
  for (auto jet : *v_jets) {
    nb_jets_signal++;
    if(fabs(jet->eta())>2.5) continue;
  }

  // ntuple + mini-xAOD outputs
  if(flag==0) {
    if( met->met() / MEV > 200. && nb_jets_signal>3) result = true;
    if( muons_n > 0 && nb_jets_signal>3) result = true;
    if( electrons_n > 0 && nb_jets_signal>3) result = true;
  }

  return (!doBaselineSelection)||result;
}
