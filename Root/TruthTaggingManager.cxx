#include "MultibjetsAnalysis/TruthTaggingManager.h"
#include <iostream>

//______________________________________________________________________________
//
TruthTaggingManager::TruthTaggingManager(){
}

//______________________________________________________________________________
//
TruthTaggingManager::~TruthTaggingManager(){
  delete m_btt;
}

//______________________________________________________________________________
//
void TruthTaggingManager::initialize( const int showerType ){
  m_btt = new BTaggingTruthTaggingTool("multib_btt");
  StatusCode code;
  code = m_btt->setProperty("TaggerName",          "MV2c10");
  if(code!=StatusCode::SUCCESS){ std::cout << "Problem in setting properties of BTaggingTruthTagging" << std::endl;}
  code =  m_btt->setProperty("OperatingPoint", "FixedCutBEff_77");
  if(code!=StatusCode::SUCCESS){ std::cout << "Problem in setting properties of BTaggingTruthTagging" << std::endl;}
  code =  m_btt->setProperty("JetAuthor", "AntiKt4EMTopoJets");
  if(code!=StatusCode::SUCCESS){ std::cout << "Problem in setting properties of BTaggingTruthTagging" << std::endl;}
  code =  m_btt->setProperty("ScaleFactorFileName", "xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-2017-01-31_v1.root");
  if(code!=StatusCode::SUCCESS){ std::cout << "Problem in setting properties of BTaggingTruthTagging" << std::endl;}
  code =  m_btt->setProperty("IgnoreScaleFactors", false);
  if(code!=StatusCode::SUCCESS){ std::cout << "Problem in setting properties of BTaggingTruthTagging" << std::endl;}
  code =  m_btt->setProperty("UsePermutations", true);
  if(code!=StatusCode::SUCCESS){ std::cout << "Problem in setting properties of BTaggingTruthTagging" << std::endl;}
  code =  m_btt->setProperty("UseQuantile", false);
  if(code!=StatusCode::SUCCESS){ std::cout << "Problem in setting properties of BTaggingTruthTagging" << std::endl;}
  code =  m_btt->setProperty("SystematicsStrategy", "SFEigen");
  if(code!=StatusCode::SUCCESS){ std::cout << "Problem in setting properties of BTaggingTruthTagging" << std::endl;}

  std::string MCshowerID = "410000";               //PowhegPy6
  if (showerType == 1) MCshowerID = "410004";      //HerwigPP
  else if (showerType == 2) MCshowerID = "410500"; //PowhegPy8
  else if (showerType == 3) MCshowerID = "410021"; //Sherpa 2.1.1
  else if (showerType == 4) MCshowerID = "410187"; //Sherpa 2.2.0
  if( m_btt->setProperty("EfficiencyBCalibrations",     MCshowerID   ) != StatusCode::SUCCESS){
    std::cout << "Problem in initialization of BTaggingTruthTagging" << std::endl;
  };
  if( m_btt->setProperty("EfficiencyCCalibrations",     MCshowerID   ) != StatusCode::SUCCESS){
    std::cout << "Problem in initialization of BTaggingTruthTagging" << std::endl;
  };
  if( m_btt->setProperty("EfficiencyTCalibrations",     MCshowerID   ) != StatusCode::SUCCESS){
    std::cout << "Problem in initialization of BTaggingTruthTagging" << std::endl;
  };
  if( m_btt->setProperty("EfficiencyLightCalibrations",     MCshowerID   ) != StatusCode::SUCCESS){
    std::cout << "Problem in initialization of BTaggingTruthTagging" << std::endl;
  };
  //initializing
  code =  m_btt->initialize();
  if(code!=StatusCode::SUCCESS){ std::cout << "Problem in initialization of BTaggingTruthTagging" << std::endl;}
  return;
}

//______________________________________________________________________________
//
void TruthTaggingManager::compute_wei(const xAOD::JetContainer*& jets, bool do_syst){
  StatusCode code;

  code =  m_btt->setJets(jets);
  if(code!=StatusCode::SUCCESS){ std::cout << "Problem in setting jets in BTaggingTruthTagging" << std::endl;}
  // clear m_TTres
  clear_ttres();

  // set seed
  int seed =0;
  for(const auto jet : *jets) {
    seed+=(int)(jet->pt()+jet->eta());
  }
  code =  m_btt->setSeed(seed);
  if(code!=StatusCode::SUCCESS){ std::cout << "Problem in setting seed in BTaggingTruthTagging" << std::endl;}

  // retreive information
  if(do_syst){
    code =  m_btt -> getTruthTagWei(4, m_TTres.ttwei_ex, m_TTres.ttwei_in);
    if(code!=StatusCode::SUCCESS){ std::cout << "Problem in getTruthTagWei in BTaggingTruthTagging" << std::endl;}
  } else {
    std::vector<double> trf_weight_ex;
    std::vector<double> trf_weight_in;
    code = m_btt->getTruthTagWei(4, trf_weight_ex, trf_weight_in, 0);
    m_TTres.ttwei_ex["Nominal"] = trf_weight_ex;
    m_TTres.ttwei_in["Nominal"] = trf_weight_in;
  }
  code =  m_btt->getTagPermutation(4, m_TTres.perm_ex, m_TTres.perm_in);
  if(code!=StatusCode::SUCCESS){
    std::cout << "Problem in getTagPermutation in BTaggingTruthTagging" << std::endl;
  }
  return;
}

//______________________________________________________________________________
//
void TruthTaggingManager::print_all(){
  int i =0;
  for(auto sys:m_TTres.ttwei_ex){
    i=0;
    std::cout << sys.first << std::endl;
    std::cout << "Esclusive" << std::endl;
    for(auto is:sys.second) std::cout << is << "  ";
    std::cout << std::endl;
    i++;
  }
  for(auto sys:m_TTres.ttwei_in){
    i=0;
    std::cout << sys.first << std::endl;
    std::cout << "Inclusive" << std::endl;
    for(auto is:sys.second) std::cout << is << "  ";
    std::cout << std::endl;
    i++;
  }
  return;
}

//______________________________________________________________________________
//
void TruthTaggingManager::clear_ttres(){
  m_TTres.ttwei_ex.clear();
  m_TTres.ttwei_in.clear();
  m_TTres.perm_ex.clear();
  m_TTres.perm_in.clear();
  m_TTres.quant_ex.clear();
  m_TTres.quant_in.clear();
  return;
}
