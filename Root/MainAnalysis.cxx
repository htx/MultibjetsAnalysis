#include "MultibjetsAnalysis/MainAnalysis.h"

#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/Worker.h"

#include "EventLoop/OutputStream.h"

#include <TTreeFormula.h>

// EDM includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODEventInfo/EventInfoAuxContainer.h"
#include "xAODEventInfo/EventAuxInfo.h"

#include "xAODTruth/TruthEventContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"

#include "xAODCore/AuxContainerBase.h"

#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"

#include "xAODBase/IParticleHelpers.h"

#include <SampleHandler/MetaFields.h>
#include <SampleHandler/MetaNames.h>

// CP and PAT tools
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "CPAnalysisExamples/errorcheck.h"
#include "SUSYTools/SUSYCrossSection.h"

// Helpers
#include "MultibjetsAnalysis/MuonHelper.h"
#include "MultibjetsAnalysis/ElectronHelper.h"
#include "MultibjetsAnalysis/JetHelper.h"
#include "MultibjetsAnalysis/TruthParticleHelper.h"
#include "MultibjetsAnalysis/TruthJetHelper.h"

//ttbar+HF classification
#include "MultibjetsAnalysis/ClassifyAndCalculateHF.h"

//ttbar+HF reweighting and systematics
#include "IFAEReweightingTools/HFSystDataMembers.h"
#include "IFAEReweightingTools/ttbbNLO_syst.h"

//top pT reweighting tool
#include "MultibjetsAnalysis/TopTtbarPtReweighting.h"

// output tree
#include "MultibjetsAnalysis/TreeMaker.h"

// selection
#include "MultibjetsAnalysis/BaselineSelection.h"

// bookkeeping!
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

#include "MultibjetsAnalysis/HelperFunctions.h"

// top tagging
#include "BoostedJetTaggers/SubstructureTopTaggerHelpers.h"

// path resolution
#include <PathResolver/PathResolver.h>

// Jet reclustering
#include <JetReclustering/JetReclusteringTool.h>

// TRF
#include "MultibjetsAnalysis/TruthTaggingManager.h"

const double GEV = 1000;

// this is needed to distribute the algorithm to the workers
ClassImp(MainAnalysis)

//______________________________________________________________________________
//
MainAnalysis :: MainAnalysis () :
m_dataSource(3),
m_doVRcJets(true),
m_doRcJets(true),
m_maxjets_mjsum(4),
m_akt4_JMS(10.0),
m_doAK10Jets(false),
m_doPileup(true),
m_doSyst(false),
m_doTruth(false),
m_doVerboseOutput(false),
m_doTopPtCorrection(false),
m_doTtbarHFClassification(0),
m_doNTUP(true),
m_doNTUPSyst(false),
m_doTRF(false),
m_doBaselineSelection(false),
m_dobTagEff(false),
m_doMM(false),
m_doxAOD(true),
m_debug(false),
m_verbose(false),
m_doQCD(false),
m_config(""),
m_JMSunc("Frozen"),
// private variable configurations below
m_event(nullptr),
m_store(nullptr),
m_isDerived(false),
m_event_count(0),
m_event_number(0),
m_run_number(0),
m_random_run_number(0),
m_channel_number(0),
m_isMC(false),
recluster_vrc_r10rho350_map(),
recluster_vrc_r10rho300_map(),
recluster_vrc_r12rho300_map(),
recluster_vrc_r10rho250_map(),
recluster_map(),
m_nb_events(0),
m_nb_events_weight(0),
m_nb_events_weight_squared(0),
top_tagger_smooth_tight(nullptr),
top_tagger_smooth_loose(nullptr),
top_tagger_lowpt_tight(nullptr),
top_tagger_lowpt_loose(nullptr),
top_tagger_highpt_tight(nullptr),
top_tagger_highpt_loose(nullptr),
m_grl(nullptr),
m_susy_tools(nullptr),
m_tree_maker(nullptr),
m_metadata_tool(nullptr),
m_trigger_metadata_tool(nullptr),
m_topttbarPtrw(nullptr),
m_tool_HFsyst(nullptr),
m_weight{8, 1},
m_cut_flow{50, 0},
m_cross_section(0),
m_rel_uncertainty(0),
m_process(0),
m_syst_info_list(),
m_syst_names(),
m_btagging_systematics()
{

}

//______________________________________________________________________________
//
EL::StatusCode MainAnalysis :: setupJob (EL::Job& job)
{
  job.useXAOD ();

  // let's initialize the algorithm to use the xAODRootAccess package
  xAOD::Init("MainAnalysis").ignore(); // call before opening first file

  // outputs
  if(m_doxAOD){
    EL::OutputStream out_xAOD ("output_xAOD", "xAOD");
    out_xAOD.options()->setString(EL::OutputStream::optMergeCmd, "xAODMerge -b"); // TODO: do we still need this???
    job.outputAdd (out_xAOD);
  }

  if(m_doNTUP){
    EL::OutputStream out_tree ("output_tree");
    job.outputAdd (out_tree);
  }

  return EL::StatusCode::SUCCESS;
}

//______________________________________________________________________________
//
EL::StatusCode MainAnalysis :: histInitialize () { return EL::StatusCode::SUCCESS; }

//______________________________________________________________________________
//
EL::StatusCode MainAnalysis :: fileExecute () {
  // set here because we need to retrieve CutBookkeepers from TEvent
  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();
  return EL::StatusCode::SUCCESS;
}

//______________________________________________________________________________
//
EL::StatusCode MainAnalysis :: changeInput (bool /*firstFile*/)
{
  const char* APP_NAME = "MainAnalysis::changeInput()";

  Info(APP_NAME, "Number of events = %lli", m_event->getEntries() );

  // get the MetaData tree once a new file is opened
  TTree *MetaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
  if (!MetaData) {
    Error("changeInput()", "MetaData not found! Exiting.");
    return EL::StatusCode::FAILURE;
  }
  MetaData->LoadTree(0);

  //check if file is from a DxAOD
  m_isDerived = !MetaData->GetBranch("StreamAOD");

  if( m_isDerived && m_dataSource!=0 /*don't do it on data*/){

    // check for corruption
    // Temporarily disabled by max to avoid crashes
    // const xAOD::CutBookkeeperContainer* incompleteCBC(nullptr);
    // CHECK(m_event->retrieveMetaInput(incompleteCBC, "IncompleteCutBookkeepers"));
    // if ( incompleteCBC->size() != 0 ) {
    //   Error("changeInput()","Found incomplete Bookkeepers! Check file for corruption. Exiting");
    //   return EL::StatusCode::FAILURE;
    // }

    // Now, let's find the actual information
    const xAOD::CutBookkeeperContainer* completeCBC(nullptr);
    CHECK(m_event->retrieveMetaInput(completeCBC, "CutBookkeepers"));

    // Now, let's actually find the right one that contains all the needed info...
    const xAOD::CutBookkeeper* allEventsCBK(nullptr);
    int maxCycle(-1);
    for (const auto& cbk: *completeCBC) {
      if (cbk->cycle() > maxCycle && cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD") {
        allEventsCBK = cbk;
        maxCycle = cbk->cycle();
      }
    }

    m_nb_events                = allEventsCBK->nAcceptedEvents();
    m_nb_events_weight         = allEventsCBK->sumOfEventWeights();
    m_nb_events_weight_squared = allEventsCBK->sumOfEventWeightsSquared();
    Info( APP_NAME, "CutBookkeepers Accepted %lu SumWei %f sumWei2 %f ", m_nb_events,  m_nb_events_weight,  m_nb_events_weight_squared);
  }

  // Keep the trigger menu in the output
  const xAOD::TriggerMenuContainer* menu(nullptr);
  CHECK(m_event->retrieveMetaInput(menu, "TriggerMenu"));

  return EL::StatusCode::SUCCESS;
}

//______________________________________________________________________________
//
EL::StatusCode MainAnalysis :: initialize ()
{
  const char* APP_NAME = "MainAnalysis::initialize()";

  Info(APP_NAME, "Number of events = %lli", m_event->getEntries() );

  // initialize SUSYTools
  m_susy_tools = new ST::SUSYObjDef_xAOD( "SUSYObjDef_xAOD" );
  // set up verbosity
  if(m_verbose) {
    m_susy_tools->msg().setLevel(MSG::VERBOSE);
    CHECK( m_susy_tools->setProperty("DebugMode", true) );
  } else if(m_debug) {
    m_susy_tools->msg().setLevel(MSG::DEBUG);
    CHECK( m_susy_tools->setProperty("DebugMode", false) );
  } else {
    m_susy_tools->msg().setLevel(MSG::ERROR);
    CHECK( m_susy_tools->setProperty("DebugMode", false) );
  }

  // convert m_dataSource to the proper enum
  ST::ISUSYObjDef_xAODTool::DataSource data_source;
  switch(m_dataSource){
    case 0:
    data_source = ST::ISUSYObjDef_xAODTool::Data;
    break;
    case 1:
    data_source = ST::ISUSYObjDef_xAODTool::FullSim;
    break;
    case 2:
    data_source = ST::ISUSYObjDef_xAODTool::AtlfastII;
    break;
    default:
    data_source = ST::ISUSYObjDef_xAODTool::Undefined;
    break;
  }

  //fill the type of sample being run over
  CHECK( m_susy_tools->setProperty("DataSource", data_source) );
  //SUSYTools configuration file
  CHECK( m_susy_tools->setProperty("ConfigFile", "MultibjetsAnalysis/"+m_config) );
  //objects used for the overlap removal
  if(m_doQCD){
    CHECK( m_susy_tools->setProperty("ORInputLabel", "baseline") );
  } else {
    CHECK( m_susy_tools->setProperty("ORInputLabel", "signal") );
  }
  // MC to MC systematics for b-tagging
  CHECK( m_susy_tools->setProperty("ShowerType", m_susy_tools->getMCShowerType(wk()->metaData()->castString(SH::MetaFields::sampleName))) );
  // JMS
  if( data_source == ST::ISUSYObjDef_xAODTool::AtlfastII ){
    CHECK( m_susy_tools->setProperty("JetJMSCalib", "") );
  } else {
    CHECK( m_susy_tools->setProperty("JetJMSCalib", m_JMSunc) );
  }

  std::string GRLPath = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/";

  std::string GRL2016 = GRLPath + "data16_13TeV/20161101/physics_25ns_20.7.xml";
  std::string GRL2015 = GRLPath + "data15_13TeV/20160720/physics_25ns_20.7.xml";

  std::string LUMI2016 = GRLPath + "data16_13TeV/20161101/physics_25ns_20.7.lumicalc.OflLumi-13TeV-005.root";
  std::string LUMI2015 = GRLPath + "data15_13TeV/20160720/physics_25ns_20.7.lumicalc.OflLumi-13TeV-005.root";

  // pileup
  if(m_doPileup){
    std::vector<std::string> prw_conf;
    prw_conf.push_back(PathResolverFindCalibFile("dev/PileupReweighting/mc15c_v2_defaults.NotRecommended.prw.root"));
    CHECK( m_susy_tools->setProperty("PRWConfigFiles", prw_conf) );

    std::vector<std::string> prw_lumicalc;
    prw_lumicalc.push_back(LUMI2016);
    prw_lumicalc.push_back(LUMI2015);
    CHECK( m_susy_tools->setProperty("PRWLumiCalcFiles", prw_lumicalc) );
  }

  CHECK(m_susy_tools->initialize());
  if(m_debug) Info(APP_NAME,"SUSYObjDef_xAOD initialized... " );

  // initialize GRL
  m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
  std::vector<std::string> vecStringGRL;
  vecStringGRL.push_back(GRL2016);
  vecStringGRL.push_back(GRL2015);
  CHECK( m_grl->setProperty( "GoodRunsListVec", vecStringGRL) );
  CHECK( m_grl->setProperty( "PassThrough", false) ); // if true (default) will ignore result of GRL and will just pass all events
  CHECK( m_grl->initialize() );
  if(m_debug) Info(APP_NAME, "GRL initialized... ");

  if(m_doAK10Jets){
    // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopTaggingPreRecommendations2015
    top_tagger_smooth_tight = STTHelpers::configSubstTagger("TightSmoothTopTag", "SmoothCut_50");
    top_tagger_smooth_loose = STTHelpers::configSubstTagger("LooseSmoothTopTag", "SmoothCut_80");
    top_tagger_lowpt_tight  = STTHelpers::configSubstTagger("TightLowPtTopTag", "FixedCut_LowPt_50");
    top_tagger_highpt_tight = STTHelpers::configSubstTagger("TightHighPtTopTag", "FixedCut_HighPt_50");
    top_tagger_lowpt_loose  = STTHelpers::configSubstTagger("LooseLowPtTopTag", "FixedCut_LowPt_80");
    top_tagger_highpt_loose = STTHelpers::configSubstTagger("LooseHighPtTopTag", "FixedCut_HighPt_80");
    if(m_debug) Info(APP_NAME, "TopTaggerTools initialized...");
  }

  // build up list of systematics
  std::vector<ST::SystInfo> cleaned_syst_info_list;
  if(m_doSyst){
    // first one is nominal
    m_syst_info_list = m_susy_tools->getSystInfoList();

    // JMR uncertainties
    CP::SystematicVariation JMR("JET_JMR",1);
    ST::SystInfo sysInfo;
    sysInfo.affectsKinematics = true;
    sysInfo.affectsWeights = false;
    sysInfo.affectedWeights.clear();
    sysInfo.affectsType = ST::SystObjType::Jet;
    sysInfo.systset.insert(JMR);
    m_syst_info_list.push_back(sysInfo);

    //clean the systematics list removing the ones we don't want anymore
    std::cout << "========================" << std::endl;
    std::cout << "Pruning some systematics: list of pruned systematics below" << std::endl;
    std::cout << "========================" << std::endl;
    for( auto syst : m_syst_info_list ){
      //skipping tau systematics
      if( ST::testAffectsObject(xAOD::Type::Tau, syst.affectsType) ){
        std::cout << "-> " << syst.systset.name() << std::endl;
        continue;
      }
      //skipping tau systematics
      if( syst.systset.name().find("PH_Iso") != std::string::npos ){
        std::cout << "-> " << syst.systset.name() << std::endl;
        continue;
      }
      cleaned_syst_info_list.push_back(syst);
    }
    std::cout << "========================" << std::endl;
    m_syst_info_list = cleaned_syst_info_list;
  } else {
    // only look at nominal
    ST::SystInfo infodef;
    infodef.affectsKinematics = false;
    infodef.affectsWeights = false;
    infodef.affectsType = ST::Unknown;
    m_syst_info_list.push_back(infodef);
  }

  // create vector of names
  for(const auto& syst : m_syst_info_list){
    if(!syst.affectsKinematics) continue;
    m_syst_names.push_back(std::string(syst.systset.name()));
    if(m_debug) Info(APP_NAME, "Adding systematic: %s", syst.systset.name().c_str());
  }

  if (m_susy_tools->resetSystematics() != CP::SystematicCode::Ok) {
    Error(APP_NAME, "Cannot reset SUSYTools systematics" );
  } else {
    Info(APP_NAME, "Reset SUSYTools systematics successfully!");
  }

  //----------
  // Trigger handling
  //----------
  m_trigger_map.clear();
  for ( const std::string file : {"trigger_menu_2015","trigger_menu_2016"} ){
    std::string item("");
    std::ifstream trigger_file;
    trigger_file.open(PathResolverFindCalibFile("MultibjetsAnalysis/"+file+".txt"));
    while (trigger_file >> item){
      m_trigger_map.insert( std::pair < std::string, bool >( item, false ) );
    }
    trigger_file.close();
  }
  m_trigger_map.insert( std::pair < std::string, bool >( "METTrigger", false ) );

  // TRF (needs to be done before initializing m_tree_maker)
  if(m_doTRF){
    m_ttm = new TruthTaggingManager();
    m_ttm -> initialize( m_susy_tools->getMCShowerType(wk()->metaData()->castString(SH::MetaFields::sampleName)) );
    m_btagging_systematics = m_ttm -> affectingSystematics();
  } else {
    m_btagging_systematics = {};
  }

  // output files
  if(m_doNTUP){
    TFile *file_tree = wk()->getOutputFile ("output_tree");
    m_tree_maker = new TreeMaker(file_tree, m_doTruth, m_doRcJets, m_doVRcJets,
      m_doAK10Jets, m_doTRF,m_btagging_systematics, m_doTtbarHFClassification,
      m_dobTagEff, m_doMM, m_doNTUPSyst, m_syst_info_list, m_trigger_map);
  }

  if(m_doxAOD){
    TFile *file_xAOD = wk()->getOutputFile ("output_xAOD");
    CHECK(m_event->writeTo(file_xAOD));

    // tools to store the meta data in the output mini-xAOD
    m_metadata_tool = new xAODMaker::FileMetaDataTool();
    if(m_debug) CHECK(m_metadata_tool->setProperty("OutputLevel", MSG::VERBOSE));
    CHECK(m_metadata_tool->initialize());
    if(m_debug) Info(APP_NAME,"FileMetaDataTool initialized... " );

    m_trigger_metadata_tool = new xAODMaker::TriggerMenuMetaDataTool();
    if(m_debug) CHECK(m_trigger_metadata_tool->setProperty("OutputLevel", MSG::VERBOSE));
    CHECK(m_trigger_metadata_tool->initialize());
    if(m_debug) Info(APP_NAME,"TriggerMenuMetaDataTool initialized... " );
  }

  if(m_doRcJets){
    // only recluster jets for things that affect kinematics (as well as nominal)
    for(const auto& sys_info : m_syst_info_list){
      std::string syst_name(sys_info.systset.name());
      // if not nominal or not affect kinematics, skip it
      if(!syst_name.empty() && !sys_info.affectsKinematics) continue;

      ANA_CHECK_SET_TYPE(EL::StatusCode);
      auto& my_tool = recluster_map[syst_name];
      //my_tool.handle().setTypeAndName("JetReclusteringTool/RC10"+syst_name);
      //ANA_CHECK(ASG_MAKE_ANA_TOOL(my_tool, JetReclusteringTool));
      my_tool.setTypeAndName("JetReclusteringTool/RC10"+syst_name);
      ANA_CHECK(my_tool.setProperty("InputJetContainer",  "SignalJets" + syst_name));
      ANA_CHECK(my_tool.setProperty("OutputJetContainer", "RC10Jets" + syst_name));
      ANA_CHECK(my_tool.setProperty("ReclusterRadius",    1.0));
      ANA_CHECK(my_tool.setProperty("VariableRMinRadius", -1.0));
      ANA_CHECK(my_tool.setProperty("VariableRMassScale", -1.0));
      ANA_CHECK(my_tool.setProperty("InputJetPtMin",      20.0));
      ANA_CHECK(my_tool.setProperty("RCJetPtMin",         150.0));
      ANA_CHECK(my_tool.setProperty("RCJetPtFrac",        0.05));
      ANA_CHECK(my_tool.setProperty("RCJetSubjetRadius",  0.4));
      ANA_CHECK(my_tool.retrieve());

      if(m_debug) my_tool->print();
    }
  }

  if(m_doVRcJets){
    // only recluster jets for things that affect kinematics (as well as nominal)
    for(const auto& sys_info : m_syst_info_list){
      std::string syst_name(sys_info.systset.name());

      // if not nominal or not affect kinematics, skip it
      if(!syst_name.empty() && !sys_info.affectsKinematics) continue;

      ANA_CHECK_SET_TYPE(EL::StatusCode);

      //===================
      auto& vrc_r10rho300_tool = recluster_vrc_r10rho300_map[syst_name];
      vrc_r10rho300_tool.setTypeAndName("JetReclusteringTool/VRC_r10rho300"+syst_name);
      ANA_CHECK(vrc_r10rho300_tool.setProperty("InputJetContainer",  "SignalJets" + syst_name));
      ANA_CHECK(vrc_r10rho300_tool.setProperty("OutputJetContainer", "VRCJets_r10rho300" + syst_name));
      ANA_CHECK(vrc_r10rho300_tool.setProperty("ReclusterRadius",    1.0));
      ANA_CHECK(vrc_r10rho300_tool.setProperty("VariableRMinRadius", 0.4));
      ANA_CHECK(vrc_r10rho300_tool.setProperty("VariableRMassScale", 300.0));
      ANA_CHECK(vrc_r10rho300_tool.setProperty("InputJetPtMin",      20.0));
      ANA_CHECK(vrc_r10rho300_tool.setProperty("RCJetPtMin",         150.0));
      ANA_CHECK(vrc_r10rho300_tool.setProperty("RCJetPtFrac",        0.05));
      ANA_CHECK(vrc_r10rho300_tool.setProperty("RCJetSubjetRadius",  0.4));
      ANA_CHECK(vrc_r10rho300_tool.retrieve());

      if(m_debug) vrc_r10rho300_tool->print();

      if(!syst_name.empty()) continue; //Only run the following configurations for nominal

      //===================
      auto& vrc_r10rho350_tool = recluster_vrc_r10rho350_map[syst_name];
      vrc_r10rho350_tool.setTypeAndName("JetReclusteringTool/VRC_r10rho350"+syst_name);
      ANA_CHECK(vrc_r10rho350_tool.setProperty("InputJetContainer",  "SignalJets" + syst_name));
      ANA_CHECK(vrc_r10rho350_tool.setProperty("OutputJetContainer", "VRCJets_r10rho350" + syst_name));
      ANA_CHECK(vrc_r10rho350_tool.setProperty("ReclusterRadius",    1.0));
      ANA_CHECK(vrc_r10rho350_tool.setProperty("VariableRMinRadius", 0.4));
      ANA_CHECK(vrc_r10rho350_tool.setProperty("VariableRMassScale", 350.0));
      ANA_CHECK(vrc_r10rho350_tool.setProperty("InputJetPtMin",      20.0));
      ANA_CHECK(vrc_r10rho350_tool.setProperty("RCJetPtMin",         150.0));
      ANA_CHECK(vrc_r10rho350_tool.setProperty("RCJetPtFrac",        0.05));
      ANA_CHECK(vrc_r10rho350_tool.setProperty("RCJetSubjetRadius",  0.4));
      ANA_CHECK(vrc_r10rho350_tool.retrieve());

      //===================
      auto& vrc_r10rho250_tool = recluster_vrc_r10rho250_map[syst_name];
      vrc_r10rho250_tool.setTypeAndName("JetReclusteringTool/VRC_r10rho250"+syst_name);
      ANA_CHECK(vrc_r10rho250_tool.setProperty("InputJetContainer",  "SignalJets" + syst_name));
      ANA_CHECK(vrc_r10rho250_tool.setProperty("OutputJetContainer", "VRCJets_r10rho250" + syst_name));
      ANA_CHECK(vrc_r10rho250_tool.setProperty("ReclusterRadius",    1.0));
      ANA_CHECK(vrc_r10rho250_tool.setProperty("VariableRMinRadius", 0.4));
      ANA_CHECK(vrc_r10rho250_tool.setProperty("VariableRMassScale", 250.0));
      ANA_CHECK(vrc_r10rho250_tool.setProperty("InputJetPtMin",      20.0));
      ANA_CHECK(vrc_r10rho250_tool.setProperty("RCJetPtMin",         150.0));
      ANA_CHECK(vrc_r10rho250_tool.setProperty("RCJetPtFrac",        0.05));
      ANA_CHECK(vrc_r10rho250_tool.setProperty("RCJetSubjetRadius",  0.4));
      ANA_CHECK(vrc_r10rho250_tool.retrieve());

      //===================
      auto& vrc_r12rho300_tool = recluster_vrc_r12rho300_map[syst_name];
      vrc_r12rho300_tool.setTypeAndName("JetReclusteringTool/VRC_r12rho300"+syst_name);
      ANA_CHECK(vrc_r12rho300_tool.setProperty("InputJetContainer",  "SignalJets" + syst_name));
      ANA_CHECK(vrc_r12rho300_tool.setProperty("OutputJetContainer", "VRCJets_r12rho300" + syst_name));
      ANA_CHECK(vrc_r12rho300_tool.setProperty("ReclusterRadius",    1.2));
      ANA_CHECK(vrc_r12rho300_tool.setProperty("VariableRMinRadius", 0.4));
      ANA_CHECK(vrc_r12rho300_tool.setProperty("VariableRMassScale", 300.0));
      ANA_CHECK(vrc_r12rho300_tool.setProperty("InputJetPtMin",      20.0));
      ANA_CHECK(vrc_r12rho300_tool.setProperty("RCJetPtMin",         150.0));
      ANA_CHECK(vrc_r12rho300_tool.setProperty("RCJetPtFrac",        0.05));
      ANA_CHECK(vrc_r12rho300_tool.setProperty("RCJetSubjetRadius",  0.4));
      ANA_CHECK(vrc_r12rho300_tool.retrieve());

      if(m_debug) vrc_r10rho350_tool->print();
      if(m_debug) vrc_r10rho250_tool->print();
      if(m_debug) vrc_r12rho300_tool->print();

    }//syst list
  }//doVRC

  //StatusCode::enableFailure();
  CP::SystematicCode::enableFailure();
  CP::CorrectionCode::enableFailure();

  //
  // Top+ttbar reweighitng
  //   -> Initializes the two tools needed for ttbar treatment: top pT reweighting and ttbar+HF reweighting
  //
  if(m_doTopPtCorrection>0 || m_doTtbarHFClassification){
    std::string dataSetName = wk()->metaData()->castString(SH::MetaFields::sampleName);
    int dsid = -1;
    //Nominal
    if( dataSetName.find(".410000.") != std::string::npos ) dsid = 410000;
    else if( dataSetName.find(".407009.") != std::string::npos ) dsid = 410000;
    else if( dataSetName.find(".407010.") != std::string::npos ) dsid = 410000;
    else if( dataSetName.find(".407011.") != std::string::npos ) dsid = 410000;
    else if( dataSetName.find(".407012.") != std::string::npos ) dsid = 410000;
    //radHi
    else if( dataSetName.find(".P2012radHi.") != std::string::npos ) dsid = 410001;
    //radLow
    else if( dataSetName.find(".P2012radLo.") != std::string::npos ) dsid = 410002;
    //aMC@NLO+Hpp
    else if( dataSetName.find(".410003.") != std::string::npos ) dsid = 410003;
    //Powheg+Hpp
    else if( dataSetName.find(".410004.") != std::string::npos ) dsid = 410004;
    else if( dataSetName.find(".PhHppEG_CT10_UE5C6L1_ttbar") != std::string::npos ) dsid = 410004;
    m_topttbarPtrw  = new TopTtbarPtReweighting(dsid);
    m_tool_HFsyst   = new ttbbNLO_syst( dataSetName, PathResolverFindCalibFile("IFAEReweightingTools/ttbbNormRw.root"),
    PathResolverFindCalibFile("IFAEReweightingTools/ttbbShapeRw.root") );
    m_tool_HFsyst -> Init();
  }

  return EL::StatusCode::SUCCESS;
}

//______________________________________________________________________________
//
EL::StatusCode MainAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  const char* APP_NAME = "MainAnalysis::execute()";

  m_event_count++;
  m_cut_flow[0]++;

  if(m_event_count % 1000 == 0) Info(APP_NAME, "On event %d", m_event_count);

  //----------------------------
  // Event information
  //---------------------------
  const xAOD::EventInfo* eventInfo(nullptr);
  CHECK(m_event->retrieve( eventInfo, "EventInfo"));
  m_event_number = eventInfo->eventNumber();
  m_run_number = eventInfo->runNumber();

  // check if the event is data or MC
  // (many tools are applied either to data or MC)
  m_isMC = eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION );

  if(m_isMC){
    //----------------------------------------------
    // pileup reweighting and cross-section for MC
    //----------------------------------------------

    m_weight[weight::mc] = (eventInfo->mcEventWeights()).at(0);

    //
    // Due to the MC weight bug, allow to use the proper weight ...
    //
    if(m_useCorrectedWeights){
      const xAOD::TruthEventContainer* truthEventContainer(nullptr);
      CHECK(m_event->retrieve( truthEventContainer, "TruthEvents"));
      if(truthEventContainer->size()>=1){
        m_weight[weight::mc] = truthEventContainer->at(0)->weights()[0];
      }
    }
    m_cut_flow[1] += m_weight[weight::mc];

    // do pileup reweighting
    if(m_doPileup){
      m_susy_tools->ApplyPRWTool();
      m_weight[weight::pileup] = m_susy_tools->GetPileupWeight();
    }
    m_cut_flow[2] += (m_weight[weight::mc] * m_weight[weight::pileup]);

    // get cross-section
    if(m_cross_section==0) {
      const xAOD::TruthParticleContainer* truth_particles(nullptr);
      CHECK( m_event->retrieve( truth_particles,"TruthParticles") );

      int id1(0), id2(0);
      CHECK( m_susy_tools->FindSusyHP(truth_particles, id1, id2) );
      if(m_verbose) std::cout << "id1 = " << id1 << " ; id2=" << id2 << std::endl;

      SUSY::CrossSectionDB myDB(PathResolverFindCalibDirectory("SUSYTools/mc15_13TeV/"));
      m_channel_number =  eventInfo->mcChannelNumber();
      m_process = SUSY::finalState(id1, id2);
      m_cross_section = myDB.xsectTimesEff(m_channel_number, m_process);
      m_rel_uncertainty = myDB.rel_uncertainty(m_channel_number, m_process);
      if(m_verbose) std::cout << m_channel_number << "  process = " << m_process << " cross-section = " <<  m_cross_section << " rel uncertainty = " << m_rel_uncertainty << std::endl;
    }
    m_run_number = m_channel_number;

    //
    //  EDIT
    //
    m_random_run_number = 0;
    if (m_doPileup) {
      if (eventInfo->isAvailable<unsigned int>("RandomRunNumber"))
      m_random_run_number = eventInfo->auxdataConst<unsigned int>("RandomRunNumber");
    } else {
      m_random_run_number = 276262;
    } //dummy 2015 run

  }//isMC
  else {
    //---------------------------------
    // GRL and event cleaning for data
    //---------------------------------

    //check if event passes GRL
    if(!m_grl->passRunLB(*eventInfo)){
      wk()->skipEvent(); // go to next event
      return EL::StatusCode::SUCCESS;
    }
    m_cut_flow[3]++;

    // remove events due to problematic regions of the detector, and incomplete events.
    if((eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error)
    || (eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error)
    || (eventInfo->errorState(xAOD::EventInfo::SCT)==xAOD::EventInfo::Error)
    || (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18)) ){
      wk()->skipEvent(); // go to the next event
      return EL::StatusCode::SUCCESS;
    }
    m_cut_flow[4]++;
  }

  long _run_number = m_isMC ? m_random_run_number : m_run_number;
  bool is_2015_period = (_run_number >= 276262) && (_run_number <= 284484);
  bool is_2016_period = (_run_number>=297730);
  if( (_run_number != 0) && !is_2015_period && !is_2016_period){
    std::cerr<<" Unknown run number "<< _run_number << std::endl;
    return EL::StatusCode::FAILURE;
  }

  //----------
  // trigger
  //----------
  //setting the individual trigger decisions to false
  for ( auto trigger : m_trigger_map ) m_trigger_map[trigger.first] = false;
  m_trigger_map["METTrigger"] = false;
  //trigger decision to use for skimming
  bool pass_trigger = false;
  // read the trigger file and build a vector of triggers to use
  std::string item("");
  std::ifstream trigger_file;
  if(is_2015_period){
    trigger_file.open(PathResolverFindCalibFile("MultibjetsAnalysis/trigger_menu_2015.txt"));
  } else {
    trigger_file.open(PathResolverFindCalibFile("MultibjetsAnalysis/trigger_menu_2016.txt"));
  }
  while (trigger_file >> item){
    const bool decision = m_susy_tools->IsTrigPassed(item);
    m_trigger_map[item] = decision;
    if(decision){
      pass_trigger = true;
    }
  }
  trigger_file.close();
  //uses the SUSYTools function to decide on the MET trigger
  m_trigger_map["METTrigger"] = m_susy_tools->IsMETTrigPassed();
  pass_trigger = (pass_trigger || m_trigger_map["METTrigger"]);

  //----------------------------------------------------------
  // Retrieve the object containers with nominal calibration
  //----------------------------------------------------------

  // Initialize the objects helpers
  MuonHelper muon_helper(m_susy_tools, m_event, m_store);
  muon_helper.SetTriggerVector( m_trigger_map );

  ElectronHelper electron_helper(m_susy_tools, m_event, m_store);
  electron_helper.SetTriggerVector( m_trigger_map );

  JetHelper jet_helper(m_susy_tools, m_event, m_store);

  TruthParticleHelper truthparticle_helper(m_event, m_store);
  TruthJetHelper truthjet_helper(m_event, m_store);

  if(m_debug) std::cout << "have helpers" << std::endl;

  //
  // Generic pointers for the nominal calibration
  //
  if(m_debug)  std::cout << "Retrieve nominal electrons... " << std::endl;
  std::pair<xAOD::ElectronContainer*, xAOD::ShallowAuxContainer*> electrons_nominal( nullptr, nullptr );
  CHECK(m_susy_tools->GetElectrons(electrons_nominal.first, electrons_nominal.second, true));
  if(m_debug) std::cout << "Nominal electrons retrieved." << std::endl;

  if(m_debug)  std::cout << "Retrieve nominal muons... " << std::endl;
  std::pair<xAOD::MuonContainer*, xAOD::ShallowAuxContainer*> muons_nominal(nullptr, nullptr);
  CHECK(m_susy_tools->GetMuons(muons_nominal.first, muons_nominal.second, true));
  if(m_debug) std::cout << "Nominal muons retrieved." << std::endl;

  if(m_debug)  std::cout << "Retrieve nominal jets... " << std::endl;
  std::pair<xAOD::JetContainer*, xAOD::ShallowAuxContainer*> jets_nominal(nullptr, nullptr);
  CHECK(m_susy_tools->GetJets(jets_nominal.first, jets_nominal.second, true));
  if(m_debug) std::cout << "Nominal jets retrieved." << std::endl;

  const xAOD::JetContainer* ak10_jets(nullptr);
  const xAOD::JetContainer* btagging_sel_jets(nullptr);

  // overlap removal
  if(m_debug)  std::cout << "Do nominal overlap removal" << std::endl;
  // SUSYTool overlap removal (mandatory to use other SUSYtools functions
  CHECK(  m_susy_tools->OverlapRemoval(electrons_nominal.first, muons_nominal.first, jets_nominal.first) );

  // MET  (keep both cst and tst)
  // hold a reference to nominal cases for when the systematics do not affect MET
  std::pair<xAOD::MissingETContainer*, xAOD::MissingETAuxContainer*> metcst_nominal(nullptr, nullptr);
  std::pair<xAOD::MissingETContainer*, xAOD::MissingETAuxContainer*> mettst_nominal(nullptr, nullptr);

  // this is the output MET container -- we will fill it with MET objects that we create so we should record this one
  std::pair<xAOD::MissingETContainer*, xAOD::MissingETAuxContainer*> mettst_out(new xAOD::MissingETContainer, new xAOD::MissingETAuxContainer);
  mettst_out.first->setStore(mettst_out.second);

  CHECK(m_store->record(mettst_out.first, "MettstOut"));
  CHECK(m_store->record(mettst_out.second, "MettstOutAux."));

  // truth particles and ttbar+HF classification
  int ttbarHF(-999), ttbarHF_ext(-999), ttbarHF_prompt(-999);
  const xAOD::TruthParticleContainer* v_truthparticles(nullptr);

  TLorentzVector v_met_truth;
  int topdecay(0),antitopdecay(0);
  double topPt(0), ttbarPt(0);
  std::map<std::string,float> mapsOfHFweights;
  HFSystDataMembers *dm_HFsyst = new HFSystDataMembers();

  double WZSherpa22_weight(1.);

  //ttbar NNLO reweighting
  TopTtbarPtReweighting::TopTtbarPtReweightingResult topptrw;
  float m_1l_weight = 1;

  if(m_isMC && m_doTruth){

    if(m_debug) std::cout << "Doing truth particles now" << std::endl;

    //
    // Ttbar + HF classification
    //
    if(m_doTtbarHFClassification){
      ClassifyAndCalculateHF classHF(m_event,"TruthParticles","AntiKt4TruthJets",true);
      classHF.apply(dm_HFsyst, ttbarHF, ttbarHF_ext, ttbarHF_prompt);
    }

    //
    // Truth information recovery
    //
    truthparticle_helper.RetrieveTruthParticles();
    truthparticle_helper.RetrieveSignalTruthParticles();
    v_truthparticles = truthparticle_helper.GetSignalTruthParticles();

    //
    // Truth jets needed for some reasons
    //
    truthjet_helper.RetrieveTruthJets();
    truthjet_helper.RetrieveSignalTruthJets();

    //
    // Top-pT reweighting
    //
    if(m_doTopPtCorrection>0 || m_doTtbarHFClassification){
      //
      // Getting the truth container, and try to do something with it ...
      //
      const xAOD::TruthParticleContainer* v_truthparticles(0);
      if(!m_event->retrieve(v_truthparticles,"TruthParticles").isSuccess()){
        Error( "MainAnalysis::execute", "Cannot retrieve TruthParticles containers !" );
        return EL::StatusCode::FAILURE;
      }
      m_topttbarPtrw -> FindTops( v_truthparticles );
      topptrw = m_topttbarPtrw -> GetEventWeight();
      m_topttbarPtrw -> GetTopTtbarPt(topPt, ttbarPt);
      dm_HFsyst -> top_pt     = topPt;
      dm_HFsyst -> ttbar_pt   = ttbarPt;
      m_1l_weight = m_topttbarPtrw -> GetTop1LWeight(topPt);
      m_topttbarPtrw -> Clear();
    }

    if(m_doTtbarHFClassification > 1){
      mapsOfHFweights = m_tool_HFsyst->GetttHFWeights(dm_HFsyst);
    }

    //
    // Defines some variables for truth ttbar studies (background composition)
    //
    topdecay        = truthparticle_helper.GetTopDecayMode(false);
    antitopdecay    = truthparticle_helper.GetTopDecayMode(true);

    // Get truth MET
    const xAOD::MissingETContainer* met_truth(nullptr);
    CHECK(m_event->retrieve(met_truth,"MET_Truth"));
    v_met_truth.SetXYZM((*met_truth)["NonInt"]->mpx()/GEV, (*met_truth)["NonInt"]->mpy()/GEV, 0, 0);

    // Compute truth MET/HT and store them in the event info
    static SG::AuxElement::Decorator<float> met_filter("met_filter");
    static SG::AuxElement::Decorator<float> ht_filter("ht_filter");
    met_filter(*eventInfo) = truthparticle_helper.truth_met_filter();
    ht_filter(*eventInfo) = truthparticle_helper.truth_ht_filter();

    if(m_debug) std::cout << "Finished doing truth particles" << std::endl;

  } // end doTruth

  //
  // W/Z+jets Sherpa 2.2 reweighting
  //
  if( m_isMC && m_doWZReweighting ){
    WZSherpa22_weight = m_susy_tools -> getSherpaVjetsNjetsWeight("AntiKt4TruthWZJets");
  }

  //---------------------------
  // Loop over all systematics
  //---------------------------

  if(m_debug) std::cout << "loop over systematics" << std::endl;
  bool pass_selection(false),
  pass_bad_jet(false),
  pass_bad_muon(false),
  pass_cosmic(false);

  for(const auto& sys_info : m_syst_info_list){
    // nominal if the systematic name is empty (the first one in the list)
    bool is_nominal(sys_info.systset.name().empty());

    // sys is easier to refer to for typing
    const CP::SystematicSet& sys = sys_info.systset;
    // quick reference to the name, we need it a lot
    std::string syst_name(sys_info.systset.name());

    if(m_debug)  std::cout << ">>>> Working on variation: \"" << syst_name << "\" <<<<<<" << std::endl;

    // Tell the SUSYObjDef_xAOD which variation to apply
    if(m_debug)  std::cout << "Call applySystematicVariation(sys)" << std::endl;

    if (m_susy_tools -> resetSystematics() != CP::SystematicCode::Ok){
      std::cout << "Could not reset to nominal at the begining of the systematics loop" << std::endl;
    }
    if (m_susy_tools->applySystematicVariation(sys) != CP::SystematicCode::Ok){
      std::cout << "Cannot configure SUSYTools for systematic variation: " << syst_name << std::endl;
    }

    // If kinematics affected, make a shallow copy with the variation applied
    bool syst_affects_electrons = ST::testAffectsObject(xAOD::Type::Electron, sys_info.affectsType);
    bool syst_affects_muons     = ST::testAffectsObject(xAOD::Type::Muon, sys_info.affectsType);
    bool syst_affects_jets      = ST::testAffectsObject(xAOD::Type::Jet, sys_info.affectsType);

    //removing the loop over weight systematics here
    if(!sys_info.affectsKinematics && !is_nominal) continue;
    //
    // // std::pair<xAOD::ElectronContainer*, xAOD::ShallowAuxContainer*> electrons(electrons_nominal);
    // // std::pair<xAOD::MuonContainer*, xAOD::ShallowAuxContainer*> muons(muons_nominal);
    // std::pair<xAOD::JetContainer*, xAOD::ShallowAuxContainer*> jets(jets_nominal);

    // always set to nominal
    std::pair<xAOD::MissingETContainer*, xAOD::MissingETAuxContainer*> metcst(metcst_nominal);
    std::pair<xAOD::MissingETContainer*, xAOD::MissingETAuxContainer*> mettst(mettst_nominal);

    std::pair<xAOD::ElectronContainer*, xAOD::ShallowAuxContainer*> electrons(nullptr,nullptr);
    std::pair<xAOD::MuonContainer*, xAOD::ShallowAuxContainer*> muons(nullptr,nullptr);
    std::pair<xAOD::JetContainer*, xAOD::ShallowAuxContainer*> jets(nullptr,nullptr);

    if(sys_info.affectsKinematics) {
      // retrieve physics objects after calibration recomputed for each systematic
      if(syst_affects_electrons) {
        // use the calibrated nominal electrons
        if(m_debug)  std::cout << "Retrieve electrons for systematics: " << syst_name << std::endl;
        CHECK(m_susy_tools->GetElectrons(electrons.first,electrons.second,false,"STCalibElectrons"));
        // record to TStore with the correct name, SUSYTools doesn't do it for Electrons
        CHECK(m_store->record(electrons.first, "STCalibElectrons" + syst_name));
        CHECK(m_store->record(electrons.second, "STCalibElectrons" + syst_name + "Aux."));
      }
      else {
        electrons = electrons_nominal;
      }
      if(syst_affects_muons) {
        // use the calibrated nominal muons
        if(m_debug)  std::cout << "Retrieve muons for systematics: " << syst_name << std::endl;
        CHECK(m_susy_tools->GetMuons(muons.first,muons.second,false,"STCalibMuons"));
        // record to TStore with the correct name, SUSYTools doesn't do it for Muons
        CHECK(m_store->record(muons.first, "STCalibMuons" + syst_name));
        CHECK(m_store->record(muons.second, "STCalibMuons" + syst_name + "Aux."));
      } else {
        muons = muons_nominal;
      }
      if(syst_affects_jets) {
        // use the calibrated nominal jets [the jetkey is to record to TStore by the name "STCalib" + jetkey_tmp + m_currentSyst.name() ]
        if(m_debug)  std::cout << "Retrieve jets for systematics: " << syst_name << std::endl;
        CHECK(m_susy_tools->GetJetsSyst(*jets_nominal.first,jets.first,jets.second,true,"AntiKt4EMTopoJets"));
      } else {
        jets = jets_nominal;
      }
    } else if (is_nominal) {
      electrons = electrons_nominal;
      muons = muons_nominal;
      jets = jets_nominal;
    }

    // it affects kinematics, so we need to use a non-nominal MET container, create a new one
    // for the nominal case, we need to make one anyway
    if(sys_info.affectsKinematics || is_nominal){
      mettst.first = new xAOD::MissingETContainer;
      mettst.second = new xAOD::MissingETAuxContainer;
      mettst.first->setStore(mettst.second);
      mettst.first->reserve(10);

      metcst.first = new xAOD::MissingETContainer;
      metcst.second = new xAOD::MissingETAuxContainer;
      metcst.first->setStore(metcst.second);
      metcst.first->reserve(10);

      if(is_nominal){
        mettst_nominal = mettst;
        metcst_nominal = metcst;
      }

      // Give memory ownership to Tstore
      CHECK(m_store->record( metcst.first, "Metcst" + syst_name ));
      CHECK(m_store->record( metcst.second, "Metcst" + syst_name + "Aux." ));
      CHECK(m_store->record( mettst.first, "Mettst" + syst_name ));
      CHECK(m_store->record( mettst.second, "Mettst" + syst_name + "Aux." ));
    }

    //overlap removal
    if(sys_info.affectsKinematics && (syst_affects_electrons || syst_affects_muons || syst_affects_jets) ) {
      if(m_debug)  std::cout << "Do overlap removal" << std::endl;
      CHECK(m_susy_tools->OverlapRemoval(electrons.first, muons.first, jets.first));
    }

    if(m_debug) std::cout << "Retrieving signal objects" << std::endl;

    // retrieve signal objects (lepton OR decoration can be affected when jets are varied)
    if(is_nominal || (sys_info.affectsKinematics && (syst_affects_muons || syst_affects_electrons || syst_affects_jets))) {
      electron_helper.DecorateBaselineElectrons(electrons.first, eventInfo, syst_name);
    }
    if(is_nominal || (sys_info.affectsKinematics && (syst_affects_electrons || syst_affects_muons || syst_affects_jets))) {
      muon_helper.DecorateBaselineMuons(muons.first, eventInfo, syst_name);
    }

    // veto events with bad muons
    pass_bad_muon = false;
    if(muon_helper.GetNbBadMuons()==0) pass_bad_muon = true;

    // veto events with cosmic muons
    pass_cosmic = true;
    //if(muon_helper.GetNbCosmicMuons()==0) pass_cosmic = true;

    if(m_debug) std::cout << "Done with retrieving signal leptons " << std::endl;

    if( is_nominal || ( sys_info.affectsKinematics && (syst_affects_jets || syst_affects_muons || syst_affects_electrons) ) ) {
      if(m_debug) std::cout << "Grab jets!" << std::endl;

      jet_helper.DecorateBaselineJets(jets.first, syst_name);

      if(m_doRcJets){
        if(m_debug){
          Info(APP_NAME, "trying to recluster for sys name %s", syst_name.c_str());
        }

        recluster_map[syst_name]->execute();
      }

      if(m_doVRcJets){
        if(m_debug){
          Info(APP_NAME, "trying to recluster with variable-R for sys name %s", syst_name.c_str());
        }
        recluster_vrc_r10rho300_map[syst_name]->execute();
        if(is_nominal){
          recluster_vrc_r10rho350_map[syst_name]->execute();
          recluster_vrc_r10rho250_map[syst_name]->execute();
          recluster_vrc_r12rho300_map[syst_name]->execute();
        }
      }

      if(is_nominal){
        if(m_debug) std::cout << "grab complicated jets " << std::endl;

        if(m_doAK10Jets){
          jet_helper.RetrieveFatJets("AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets",
          top_tagger_smooth_tight,
          top_tagger_smooth_loose,
          top_tagger_lowpt_tight,
          top_tagger_lowpt_loose,
          top_tagger_highpt_tight,
          top_tagger_highpt_loose);//AntiKt10LCTopoJets");
          //xAOD::JetContainer* fat_jets(nullptr);
          //xAOD::ShallowAuxContainer* fat_jets_aux(nullptr);
          // jet_helper.RetrieveFatJetsST(fat_jets, fat_jets_aux);
          jet_helper.RetrieveSignalFatJets();
          ak10_jets = jet_helper.GetSignalFatJets();
        }
        if(m_debug) std::cout << "done with jets " << std::endl;

      } // end is nominal

      // veto events with bad jets
      pass_bad_jet = false;
      if(jet_helper.GetNbBadJets()==0) pass_bad_jet = true;

      // TRF
      btagging_sel_jets = jet_helper.GetSignalBTaggableJets(is_nominal,
                          sys_info.affectsKinematics && (syst_affects_jets || syst_affects_muons || syst_affects_electrons),
                          syst_name);
      if(m_debug){
        std::cout << "Number of jets considered: " << btagging_sel_jets->size() << std::endl;
      }
      if(m_doTRF) {
        m_ttm->compute_wei(btagging_sel_jets, m_doSyst);
        if(m_debug){
          m_ttm->print_all();
        }
      }
    } // end is jets

    // met calculation
    if(is_nominal || sys_info.affectsKinematics) {
      if(m_debug)  std::cout << "Do MET calculation" << std::endl;
      if(m_doQCD){
        CHECK(m_susy_tools->GetMET(*metcst.first,
          jet_helper.GetBaselineJets(is_nominal, sys_info.affectsKinematics && (syst_affects_jets || syst_affects_electrons || syst_affects_muons), syst_name),
          electron_helper.GetBaselineElectrons(is_nominal, sys_info.affectsKinematics && (syst_affects_electrons || syst_affects_jets || syst_affects_muons), syst_name, m_store),
          muon_helper.GetBaselineMuons(is_nominal, sys_info.affectsKinematics && (syst_affects_muons || syst_affects_jets || syst_affects_electrons), syst_name, m_store),0, 0, false));
        CHECK(m_susy_tools->GetMET(*mettst.first,
          jet_helper.GetBaselineJets(is_nominal, sys_info.affectsKinematics && (syst_affects_jets || syst_affects_electrons || syst_affects_muons), syst_name),
          electron_helper.GetBaselineElectrons(is_nominal, sys_info.affectsKinematics && (syst_affects_muons || syst_affects_electrons || syst_affects_jets), syst_name, m_store),
          muon_helper.GetBaselineMuons(is_nominal, sys_info.affectsKinematics && (syst_affects_muons || syst_affects_jets || syst_affects_electrons), syst_name, m_store), 0, 0, true));
      } else {
        CHECK(m_susy_tools->GetMET(*metcst.first,
          jet_helper.GetBaselineJets(is_nominal, sys_info.affectsKinematics && (syst_affects_jets || syst_affects_electrons || syst_affects_muons), syst_name),
          electron_helper.GetSignalElectrons(is_nominal, sys_info.affectsKinematics && (syst_affects_electrons || syst_affects_jets || syst_affects_muons), syst_name, m_store),
          muon_helper.GetSignalMuons(is_nominal, sys_info.affectsKinematics && (syst_affects_muons || syst_affects_jets || syst_affects_electrons), syst_name, m_store),0, 0, false));
        CHECK(m_susy_tools->GetMET(*mettst.first,
          jet_helper.GetBaselineJets(is_nominal, sys_info.affectsKinematics && (syst_affects_jets || syst_affects_electrons || syst_affects_muons), syst_name),
          electron_helper.GetSignalElectrons(is_nominal, sys_info.affectsKinematics && (syst_affects_muons || syst_affects_electrons || syst_affects_jets), syst_name, m_store),
          muon_helper.GetSignalMuons(is_nominal, sys_info.affectsKinematics && (syst_affects_muons || syst_affects_jets || syst_affects_electrons), syst_name, m_store), 0, 0, true));
      }
    }

    //-----------------------
    // Get all scale factors
    //-----------------------
    std::map < const std::string, Double_t > weight_map;

    //pileup and MC weights
    weight_map.insert( std::pair < const std::string, Double_t >("weight_mc",  m_weight[weight::mc]) );
    weight_map.insert( std::pair < const std::string, Double_t >("weight_pu",  m_weight[weight::pileup]) );

    //Get b-tagging scale factors
    if(m_debug)  std::cout << "Get b-tagging weights" << std::endl;
    if(m_isMC) m_weight[weight::btaggingSF] = m_susy_tools->BtagSF(jet_helper.GetSignalBTaggableJets(is_nominal, sys_info.affectsKinematics && (syst_affects_jets || syst_affects_electrons || syst_affects_muons), syst_name));
    weight_map.insert( std::pair < const std::string, Double_t >("weight_btag",  m_weight[weight::btaggingSF]) );

    //Get electron scale factors
    if(m_debug)  std::cout << "Get electrons weights" << std::endl;
    // booleans: reco, ID, Trigger, Iso scale factors
    if(m_isMC) m_weight[weight::elecSF] = m_susy_tools->GetTotalElectronSF(*electrons.first,true,true, false /*all SFs but the trigger ones*/, true);
    weight_map.insert( std::pair < const std::string, Double_t >("weight_elec",  m_weight[weight::elecSF]) );
    if(m_isMC) m_weight[weight::elecTrigSF] = m_susy_tools->GetTotalElectronSF(*electrons.first,false,false,true,false);//only trigger SFs
    weight_map.insert( std::pair < const std::string, Double_t >("weight_elec_trigger",  m_weight[weight::elecTrigSF]) );

    //Get muon scale factors
    // reco SF = true; iso SF = true
    if(m_debug)  std::cout << "Get muons weights" << std::endl;
    if(m_isMC) m_weight[weight::muonSF] = m_susy_tools->GetTotalMuonSF(*muons.first,true,true, "");
    weight_map.insert( std::pair < const std::string, Double_t >("weight_muon", m_weight[weight::muonSF]) );
    if(m_isMC){
      const double muon_weight = m_susy_tools->GetTotalMuonSF(*muons.first,false,false, is_2016_period ? "HLT_mu26_ivarmedium_OR_HLT_mu50" : "HLT_mu20_iloose_L1MU15_OR_HLT_mu50");
      m_weight[weight::muonTrigSF] = (muon_weight==0) + muon_weight;
    }
    weight_map.insert( std::pair < const std::string, Double_t >("weight_muon_trigger",  m_weight[weight::muonTrigSF]) );

    // Get JVT SF's
    if(m_isMC) m_weight[weight::jvtSF] = m_susy_tools->JVT_SF(jet_helper.GetBaselineJets(is_nominal, sys_info.affectsKinematics && (syst_affects_jets || syst_affects_electrons || syst_affects_muons), syst_name));
    weight_map.insert ( std::pair < const std::string, Double_t >("weight_jvt",  m_weight[weight::jvtSF]) );

    // Get NNLO ttbar weight
    if(m_isMC && m_doTopPtCorrection){
      weight_map.insert ( std::pair < const std::string, Double_t >("weight_ttbar_NNLO",  topptrw.nominal) );
      weight_map.insert ( std::pair < const std::string, Double_t >("weight_ttbar_NNLO_1L",  m_1l_weight) );
    }

    // Get W/Z+jets Sherpa 2.2 reweighting
    if(m_isMC && m_doWZReweighting){
      weight_map.insert ( std::pair < const std::string, Double_t >("weight_WZ_2_2",  WZSherpa22_weight) );
    }

    // If nominal, get also all weights for systematics
    if(is_nominal) {
      // loop over systematics to get the relevant scale factors
      for(const auto& sys_info_weight : m_syst_info_list){
        const CP::SystematicSet& sys_weight = sys_info_weight.systset;

        if(sys_info_weight.affectsKinematics || (sys_weight.name()).empty()) continue;

        // Tell the SUSYObjDef_xAOD which variation to apply
        if(m_debug)  std::cout << "Call applySystematicVariation(sys)" << std::endl;
        if (m_susy_tools->applySystematicVariation(sys_weight) != CP::SystematicCode::Ok){
          std::cout << "Cannot configure SUSYTools for systematic variation " << (sys_weight.name()).c_str() << std::endl;
        }

        bool syst_weight_affects_electrons = ST::testAffectsObject(xAOD::Type::Electron, sys_info_weight.affectsType);
        bool syst_weight_affects_muons     = ST::testAffectsObject(xAOD::Type::Muon, sys_info_weight.affectsType);
        bool syst_weight_affects_bTag      = ST::testAffectsObject(xAOD::Type::BTag, sys_info_weight.affectsType);
        bool syst_weight_affects_JVT       = (sys_weight.name().find("Jvt") != std::string::npos);
        bool syst_weight_affects_PU        = (sys_weight.name().find("PRW") != std::string::npos);
        bool syst_is_trigger               = (sys_weight.name().find("EFF_Trig") != std::string::npos);

        if(syst_weight_affects_electrons){
          if(!syst_is_trigger){
            //ID/RECO/ISO
            float weight =  m_susy_tools->GetTotalElectronSF(*electrons.first,true,true,true,true);
            weight_map.insert( std::pair < const std::string, Double_t >("weight_elec_"+sys_weight.name(),  weight) );
          } else {
            //TRIGGER
            float weight =  m_susy_tools->GetTotalElectronSF(*electrons.first,false,false,true,false);
            weight_map.insert( std::pair < const std::string, Double_t >("weight_elec_trigger_"+sys_weight.name(),  weight) );
          }
        }
        else if(syst_weight_affects_muons){
          if(!syst_is_trigger){
            float weight =  m_susy_tools->GetTotalMuonSF(*muons.first,true,true, "");
            weight_map.insert( std::pair < const std::string, Double_t >("weight_muon_"+sys_weight.name(),  weight) );
          } else {
            float weight = m_susy_tools->GetTotalMuonSF(*muons.first,false,false, is_2016_period ? "HLT_mu26_ivarmedium_OR_HLT_mu50" : "HLT_mu20_iloose_L1MU15_OR_HLT_mu50");
            weight_map.insert( std::pair < const std::string, Double_t >("weight_muon_trigger_"+sys_weight.name(),  (weight==0)+weight) );
          }
        }
        else if(syst_weight_affects_bTag) {
          float weight = m_susy_tools->BtagSF(jet_helper.GetSignalBTaggableJets(is_nominal, false, syst_name));
          weight_map.insert( std::pair < const std::string, Double_t >("weight_btag_"+sys_weight.name(),  weight) );
        }
        else if(syst_weight_affects_JVT) {
          float weight = m_susy_tools->JVT_SF(jet_helper.GetBaselineJets(is_nominal, false, syst_name));
          weight_map.insert( std::pair < const std::string, Double_t >("weight_jvt_"+sys_weight.name(),  weight) );
        }
        else if(syst_weight_affects_PU) {
          float weight = m_susy_tools->GetPileupWeight();
          weight_map.insert( std::pair < const std::string, Double_t >("weight_pu_"+sys_weight.name(),  weight) );
        }
      }
      if (m_susy_tools -> resetSystematics() != CP::SystematicCode::Ok){
        std::cout << "Could not reset to nominal after weight systematics loop" << std::endl;
      }
    }

    //-----------------------------------------------------------------------
    // Fill ntuples for events which pass the selection
    //-----------------------------------------------------------------------
    if(is_nominal || sys_info.affectsKinematics) {

      if(m_debug) std::cout << "test for baseline selection" << std::endl;

      // keep event if satisfies the selection cuts for nominal
      pass_selection = false;
      if( BaselineSelection::keep(m_doBaselineSelection, mettst.first,
          jet_helper.GetSignalJets(is_nominal,  sys_info.affectsKinematics && (syst_affects_jets || syst_affects_electrons || syst_affects_muons), syst_name),
          muon_helper.GetBaselineMuons(is_nominal, sys_info.affectsKinematics && (syst_affects_electrons || syst_affects_jets || syst_affects_muons), syst_name, m_store),
				  electron_helper.GetBaselineElectrons(is_nominal, sys_info.affectsKinematics && (syst_affects_muons || syst_affects_jets || syst_affects_electrons ), syst_name, m_store),m_doQCD,0) ) {
        if(m_debug) std::cout << " passed baseline for ntuples and mini-xAODs" << std::endl;
        pass_selection = true;
      }

      if(m_doNTUP && pass_selection && pass_bad_jet && pass_bad_muon && pass_cosmic && pass_trigger){
        if( is_nominal || m_doNTUPSyst ) {
          if(m_debug) std::cout << "recording ntuples" << std::endl;
          if(m_debug) std::cout << "start grabbing tree info" << std::endl;

          float average_interactions_per_crossing = eventInfo->averageInteractionsPerCrossing();
          float actual_interactions_per_crossing = eventInfo->actualInteractionsPerCrossing();
          int n_primary_vertices = HelperFunctions::getNVtx(m_event);
          const xAOD::Vertex* pv = m_susy_tools->GetPrimVtx();
          float primary_vertex_z = pv ? pv->z() : 0;

          if(m_debug) std::cout << " before tree fill standard " << std::endl;

          if(!is_nominal && m_debug){
            std::cout << syst_name << std::endl;
          }

          TTres * ttres_to_pass =0;
          if(m_doTRF) ttres_to_pass = &(m_ttm->m_TTres);

          m_tree_maker->Fill_obj(m_event_number, m_run_number, m_random_run_number, average_interactions_per_crossing,
            actual_interactions_per_crossing, primary_vertex_z, n_primary_vertices, m_process, ttbarHF, ttbarHF_ext,
            ttbarHF_prompt,mapsOfHFweights,topdecay,antitopdecay,
            muon_helper.GetBaselineMuons(is_nominal, sys_info.affectsKinematics && (syst_affects_jets || syst_affects_muons || syst_affects_electrons), syst_name, m_store),
            electron_helper.GetBaselineElectrons(is_nominal, sys_info.affectsKinematics && (syst_affects_jets || syst_affects_electrons || syst_affects_muons), syst_name, m_store),
            jet_helper.GetSignalJets(is_nominal, sys_info.affectsKinematics && (syst_affects_jets || syst_affects_electrons || syst_affects_muons), syst_name),
            metcst.first,mettst.first,m_trigger_map,weight_map,m_doTRF, ttres_to_pass,eventInfo,m_event);

          if(m_debug) std::cout << "after tree fill standard " << std::endl;

          if(m_doRcJets){
            m_tree_maker->Fill_rc_jets(m_store, syst_name, is_nominal,sys_info.affectsKinematics && ( syst_affects_jets || syst_affects_electrons || syst_affects_muons ), m_maxjets_mjsum);
          }
          if(m_doVRcJets){
            m_tree_maker->Fill_vrc_jets(m_store, syst_name, is_nominal,sys_info.affectsKinematics && ( syst_affects_jets || syst_affects_electrons || syst_affects_muons ), m_maxjets_mjsum);
          }
          if(m_debug) std::cout << " after tree fill rc" << std::endl;

          if(is_nominal) {
            if(m_isMC && m_doTruth) m_tree_maker->Fill_truth(v_truthparticles, v_met_truth/*, met_truth_filter*/);
            if(m_debug) std::cout << "after tree fill truth " << std::endl;

            if(m_doAK10Jets) m_tree_maker->Fill_ak10_jets(ak10_jets);
            if(m_debug) std::cout << "after tree fill ak10" << std::endl;

            if(m_dobTagEff) m_tree_maker->Fill_bTag_efficiencies(jet_helper.GetSignalJets(is_nominal, sys_info.affectsKinematics && (syst_affects_jets || syst_affects_electrons || syst_affects_muons), syst_name));
            if(m_debug) std::cout << "after tree fill b-tagging efficiencies" << std::endl;

            if(m_doMM) m_tree_maker->Fill_matrix_method(jet_helper.GetSignalJets(is_nominal, sys_info.affectsKinematics && (syst_affects_jets || syst_affects_electrons || syst_affects_muons), syst_name));
            if(m_debug) std::cout << "after tree fill matrix method weights" << std::endl;
          }

          m_tree_maker->Fill((syst_name.empty())?"nominal":syst_name);
          if(m_debug) std::cout << "after full tree " << std::endl;
        } // ( is_nominal || m_doNTUPSyst ) )
      } // end baseline selection
    } // end nominal or sys affects kinematics

    if(is_nominal){
      xAOD::MissingETContainer::const_iterator mettst_it = mettst.first->find("Final");
      if (mettst_it == mettst.first->end()) std::cout << "No RefFinal inside MET container" << std::endl;
      xAOD::MissingET* out_met = new xAOD::MissingET();

      mettst_out.first->push_back(out_met);
      *out_met = **mettst_it; // copies kinematics, name, source
      out_met->setName((*mettst_it)->name());
    }

    if(sys_info.affectsKinematics) {
      // create and copy a new MET term
      xAOD::MissingETContainer::const_iterator mettst_it = mettst.first->find("Final");
      if (mettst_it == mettst.first->end()) std::cout << "No RefFinal inside MET container" << std::endl;
      xAOD::MissingET* out_met = new xAOD::MissingET();
      mettst_out.first->push_back(out_met);
      *out_met = **mettst_it; // copies kinematics, name, source
      out_met->setName((*mettst_it)->name()+'_'+syst_name);
    }

  } // end loop over systematics

  if (m_susy_tools -> resetSystematics() != CP::SystematicCode::Ok){
    std::cout << "Could not reset to nominal after systematics loop" << std::endl;
  }

  if(m_debug) std::cout << "End loop over systematics" << std::endl;

  if(pass_trigger)  m_cut_flow[5]++;
  if(pass_bad_jet) m_cut_flow[6]++;
  if(pass_bad_muon) m_cut_flow[7]++;
  if(pass_cosmic) m_cut_flow[8]++;
  if(pass_selection)  m_cut_flow[9]++;

  // Fill mini-xAOD outputs for events which pass the selection
  if(!(pass_selection && pass_bad_jet && pass_bad_muon && pass_cosmic && pass_trigger)) {
    if(m_debug) std::cout << "did not pass selections, end of event!" << std::endl;
    wk()->skipEvent();
    return EL::StatusCode::SUCCESS;
  }

  // make xAOD
  if(m_doxAOD) {
    if(m_isMC && m_doTruth){
      if(m_debug) std::cout << "decorate EventInfo with various truth information" << std::endl;
      static SG::AuxElement::Decorator<int> ttbar_class("ttbar_class");
      static SG::AuxElement::Decorator<int> ttbar_class_ext("ttbar_class_ext");
      static SG::AuxElement::Decorator<float> met_truth_filter_dec("met_truth_filter");
      static SG::AuxElement::Decorator<float> topdecay_dec("topdecay");
      static SG::AuxElement::Decorator<float> antitopdecay_dec("antitopdecay");
      ttbar_class(*eventInfo) = ttbarHF;
      ttbar_class_ext(*eventInfo) = ttbarHF_ext;
      //met_truth_filter_dec(*eventInfo) = met_truth_filter;
      topdecay_dec(*eventInfo) = topdecay;
      antitopdecay_dec(*eventInfo) = antitopdecay;

      if(mapsOfHFweights.size()>0)
      for( const auto& ttbb_weight : mapsOfHFweights )
      eventInfo->auxdecor < float > (ttbb_weight.first.c_str()) = ttbb_weight.second;

      // copy over the truth information we need
      CHECK(m_event->copy("TruthVertices"));
      CHECK(m_event->copy("TruthEvents"));
      truthparticle_helper.MakeDeepCopy();
    }

    if(m_debug) std::cout << "decorate EventInfo with various weights" << std::endl;
    static SG::AuxElement::Decorator<float> weight_mc("weight_mc");
    static SG::AuxElement::Decorator<float> weight_pu("weight_pu");
    static SG::AuxElement::Decorator<float> weight_btag("weight_btag");
    weight_mc(*eventInfo) = m_weight[weight::mc];
    weight_pu(*eventInfo) = m_weight[weight::pileup];
    weight_btag(*eventInfo) = m_weight[weight::btaggingSF];

    if(m_debug) std::cout << "recording xAOD" << std::endl;

    CHECK(m_event->copy("EventInfo"));
    if(m_debug) std::cout << "did EventInfo" << std::endl;

    // we want trigger decisions in the output
    //  make sure metadata is also copied using xAODTrigCnv by initializing tool
    CHECK(m_event->copy("xTrigDecision"));
    CHECK(m_event->copy("TrigConfKeys"));
    if(m_debug) std::cout << "did trigger information" << std::endl;

    CHECK(m_event->record(mettst_out.first, "Mettst"));
    CHECK(m_event->record(mettst_out.second, "MettstAux."));
    if(m_debug) std::cout << "did MET" << std::endl;

    muons_nominal.second->setShallowIO(false);
    CHECK(m_event->record(muons_nominal.first, "STCalibMuons"));
    CHECK(m_event->record(muons_nominal.second, "STCalibMuonsAux."));
    if(m_debug) std::cout << "did muons" << std::endl;

    electrons_nominal.second->setShallowIO(false);
    CHECK(m_event->record(electrons_nominal.first, "STCalibElectrons"));
    CHECK(m_event->record(electrons_nominal.second, "STCalibElectronsAux."));
    if(m_debug) std::cout << "did electrons" << std::endl;

    CHECK(m_event->copy("BTagging_AntiKt4EMTopo"));
    jets_nominal.second->setShallowIO(false);
    CHECK(m_event->record(jets_nominal.first, "STCalibAntiKt4EMTopoJets"));
    CHECK(m_event->record(jets_nominal.second, "STCalibAntiKt4EMTopoJetsAux."));
    if(m_debug) std::cout << "did jets" << std::endl;

    CHECK(m_event->fill());
    if(m_debug) std::cout << "done with TEvent::fill()" << std::endl;
  }

  if(m_debug) std::cout << "did pass selections, end of event!" << std::endl;

  return EL::StatusCode::SUCCESS;
}


//______________________________________________________________________________
//
EL::StatusCode MainAnalysis :: postExecute () { return EL::StatusCode::SUCCESS; }


//______________________________________________________________________________
//
EL::StatusCode MainAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  const char* APP_NAME = "MainAnalysis::finalize()";

  // get the sum of weights from the pileup reweighting
  double sum_weights = 0;
  if(m_isMC && m_doPileup) sum_weights = m_susy_tools->GetSumOfWeights(m_channel_number);
  std::cout << "Sum weights from pileup tool = " << sum_weights << std::endl;

  //Delete top-pt reweighting tool
  if( m_topttbarPtrw && ( m_doTopPtCorrection>0 || m_doTtbarHFClassification ) ) {
    delete m_topttbarPtrw;
    delete m_tool_HFsyst;
  }
  // If no metadat, put the initial number of events to the number of events in the file
  if(m_nb_events==0){
    m_nb_events = m_cut_flow[0];
    m_nb_events_weight = m_cut_flow[0];
  }

  if(m_doNTUP){
    // output tree
    TFile *file_tree = wk()->getOutputFile ("output_tree");
    //m_tree_maker->Write();
    TH1D *h_cut_flow_tree = new TH1D("cut_flow", "number of events no weight",20, 0,20 );
    // the first 2 bins are the initial and final number of events in the input file
    h_cut_flow_tree->GetXaxis()->SetBinLabel(1,"Initial unweighted events");
    h_cut_flow_tree->Fill(0.5,m_nb_events);
    h_cut_flow_tree->GetXaxis()->SetBinLabel(2,"Initial weighted events (CBK)");
    h_cut_flow_tree->Fill(1.5,m_nb_events_weight);
    h_cut_flow_tree->GetXaxis()->SetBinLabel(3,"Total weight from pile-up tool");
    h_cut_flow_tree->Fill(2.5,sum_weights);
    h_cut_flow_tree->GetXaxis()->SetBinLabel(4,"Total weight with MC weight");
    h_cut_flow_tree->Fill(3.5,m_cut_flow[1]);
    h_cut_flow_tree->GetXaxis()->SetBinLabel(5,"Total weight with MC weight and pile-up");
    h_cut_flow_tree->Fill(4.5,m_cut_flow[2]);
    for(int icut=0; icut<10; icut++) h_cut_flow_tree->Fill(icut+9.5,m_cut_flow[icut]);
    h_cut_flow_tree->SetDirectory (file_tree);
    TH1D *h_cross_section_tree = new TH1D("cross_section", "cross_Section",1, 0,1 );
    h_cross_section_tree->Fill(0.5,m_cross_section);
    h_cross_section_tree->SetDirectory (file_tree);
  }

  if(m_doxAOD){
    // output xAOD
    TFile *file_xAOD = wk()->getOutputFile ("output_xAOD");
    CHECK(m_event->finishWritingTo( file_xAOD ));
    TH1D *h_cut_flow_xAOD = new TH1D("cut_flow", "number of events no weight",20, 0,20 );
    // the first 2 bins are the initial and final number of events in the input file
    h_cut_flow_xAOD->Fill(0.5,m_nb_events);
    h_cut_flow_xAOD->Fill(1.5,m_nb_events_weight);
    h_cut_flow_xAOD->Fill(2.5,sum_weights);
    h_cut_flow_xAOD->Fill(3.5,m_cut_flow[1]);
    h_cut_flow_xAOD->Fill(4.5,m_cut_flow[2]);
    for(int icut=0; icut<10; icut++) h_cut_flow_xAOD->Fill(icut+9.5,m_cut_flow[icut]);
    h_cut_flow_xAOD->SetDirectory (file_xAOD);
    TH1D *h_cross_section_xAOD = new TH1D("cross_section", "cross_Section",1, 0,1 );
    h_cross_section_xAOD->Fill(0.5,m_cross_section);
    h_cross_section_xAOD->SetDirectory (file_xAOD);

    // cut flow
    for(u_int i=0 ; i<15;i++) std::cout << "Cut " << i << " nb events = " << m_cut_flow[i] << std::endl;

    //TFile *file_xAOD = wk()->getOutputFile ("output_xAOD");
    file_xAOD->cd();
    TTree* manual_meta = new TTree("manual_meta", "manual metadata tree");
    manual_meta->Branch("systematic_names",&m_syst_names);
    manual_meta->Fill();
    manual_meta->Write();
    delete manual_meta;
  }

  // GRL
  if( m_grl ) delete m_grl;

  //SUSY ObjDef-xAOD
  if( m_susy_tools ) delete m_susy_tools;

  // TRF
  if(m_doTRF) delete m_ttm;

  return EL::StatusCode::SUCCESS;
}

//______________________________________________________________________________
//
EL::StatusCode MainAnalysis :: histFinalize () { return EL::StatusCode::SUCCESS; }
