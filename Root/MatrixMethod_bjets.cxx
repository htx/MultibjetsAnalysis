#include "MultibjetsAnalysis/MatrixMethod_bjets.h"

using namespace std;

//float MatrixMethod_bjets::m_ptbins[5] = {30,50,80,130,200};

float MatrixMethod_bjets::m_ptbins[7] = {30,40,50,70,100,150,200};
float MatrixMethod_bjets::m_etabins[3] = {0.,1.2,2.5};

MatrixMethod_bjets::MatrixMethod_bjets()
{
}


void MatrixMethod_bjets::initialize(std::string filename)
{

    m_file = new TFile(filename.c_str());
    char *histname = new char[256];
    /*
    for(unsigned int i=0;i<3;i++)
    {
        std::string b_weight = "";
        if(i==1) b_weight = "_down";
        else if(i==2) b_weight = "_up";

        sprintf(histname, "eff_C%s",b_weight.c_str());
        h_eff_C[i] = (TH2F*)m_file->Get(histname);
        h_eff_C[i]->SetDirectory(0);

	sprintf(histname, "eff_L%s",b_weight.c_str());
        h_eff_L[i] = (TH2F*)m_file->Get(histname);
        h_eff_L[i]->SetDirectory(0);

        sprintf(histname, "eff_T%s",b_weight.c_str());
        h_eff_T[i] = (TH2F*)m_file->Get(histname);
        h_eff_T[i]->SetDirectory(0);
    }

    char plot_name[556];
    for(int ijet=0;ijet<7;ijet++)
    {
        sprintf(plot_name,"frac_C_%d",ijet);
        h_frac_C_eta_pt[ijet] = (TH2F*)m_file->Get(plot_name);
        h_frac_C_eta_pt[ijet]->SetDirectory(0);

        sprintf(plot_name,"frac_L_%d",ijet);
        h_frac_L_eta_pt[ijet] = (TH2F*)m_file->Get(plot_name);
        h_frac_L_eta_pt[ijet]->SetDirectory(0);

        sprintf(plot_name,"frac_T_%d",ijet);
        h_frac_T_eta_pt[ijet] = (TH2F*)m_file->Get(plot_name);
        h_frac_T_eta_pt[ijet]->SetDirectory(0);

        sprintf(plot_name,"frac_MCstat_C_%d",ijet);
        h_frac_C_eta_pt_MCstat[ijet] = (TH2F*)m_file->Get(plot_name);
        h_frac_C_eta_pt_MCstat[ijet]->SetDirectory(0);

        sprintf(plot_name,"frac_MCstat_L_%d",ijet);
        h_frac_L_eta_pt_MCstat[ijet] = (TH2F*)m_file->Get(plot_name);
        h_frac_L_eta_pt_MCstat[ijet]->SetDirectory(0);

        sprintf(plot_name,"frac_MCstat_T_%d",ijet);
        h_frac_T_eta_pt_MCstat[ijet] = (TH2F*)m_file->Get(plot_name);
        h_frac_T_eta_pt_MCstat[ijet]->SetDirectory(0);
    }

    h_frac_C_mT = (TH1F*)m_file->Get("frac_mT_C");
    h_frac_C_mT->SetDirectory(0);

    h_frac_L_mT = (TH1F*)m_file->Get("frac_mT_L");
    h_frac_L_mT->SetDirectory(0);

    h_frac_T_mT = (TH1F*)m_file->Get("frac_mT_T");
    h_frac_T_mT->SetDirectory(0);
    */
    // fake rates from data
    //for(int i=0;i<2;i++)
    for(int i=0;i<1;i++)
      {
        sprintf(histname, "eff_fake_%d",i);
        eff_fake_b[i] = (TH2F*)m_file->Get(histname);
        eff_fake_b[i]->SetDirectory(0);

        sprintf(histname, "eff_fake_stat_%d",i);
        eff_fake_b_stat[i] = (TH2F*)m_file->Get(histname);
        eff_fake_b_stat[i]->SetDirectory(0);
    }

}

int MatrixMethod_bjets::Combination(int i, int ip)
{
    int r =  TMath::Factorial(i)/( TMath::Factorial(ip)* TMath::Factorial(i-ip));
    return r;
}


float MatrixMethod_bjets::GetFakeEfficiency(float eta, float pt, int /*nJets*/, float /*mt*/, int syst)
{

    float eff_fake = 0;

    // Definition of systematics
    // 0 : nominal
    // 1 : B-down
    // 2 : C and T down
    // 3 : L-down
    // 4 : B-up
    // 5 : C and T up
    // 6 : L-up
    // 7 : MC stat
    // 8 : data

    //int syst_bin_CT = 0;
    //if(syst==2) syst_bin_CT = 1;
    //else if(syst==5) syst_bin_CT = 2;

    //int syst_bin_L = 0;
    //if(syst==3) syst_bin_L = 1;
    //else if(syst==6) syst_bin_L = 2;

    /// search the corresponding eta bin
    eta = fabs(eta);
    int eta_bin = -1;
    for(int i=0; i<2; ++i)
    {
        if( (eta >= m_etabins[i]) && (eta < m_etabins[i+1]) )
        {
            eta_bin = i+1;
            break;
        }
    }
    if(eta_bin==-1) return 0.;

    /// search the corresponding pt bin
    int pt_bin = -1;
    for(int i=0; i<6; ++i)
    {
        if( (pt >= m_ptbins[i]) && (pt < m_ptbins[i+1]) )
        {
            pt_bin = i+1;
            break;
        }
        else if( pt >= m_ptbins[6])
        {
            pt_bin = 7;
            break;
        }
    }
    if(pt_bin==-1) return 0.;

    // if(syst==8) // get the fake rate from data
    //{
        int mT_bin = 0;
        //if(mt>100.) mT_bin = 1;

        eff_fake = eff_fake_b[mT_bin]->GetBinContent(eta_bin,pt_bin);

	/*}
    else { // get the nomilal fake rate from MC with nJets and mT parameterisation
        /// search the corresponding jet and mT bins
        if(mt>495) mt = 495;
        int mT_bin = ceil(mt/10.);
        int nJets_bin = 0;
        if(nJets==5) nJets_bin = 1;
        else if(nJets==6) nJets_bin = 2;
        else if(nJets==7) nJets_bin = 3;
        else if(nJets==8) nJets_bin = 4;
        else if(nJets==9) nJets_bin = 5;
        else if(nJets>9) nJets_bin = 6;


	float eff_C = h_eff_C[syst_bin_CT]->GetBinContent(eta_bin,pt_bin);
	float eff_L = h_eff_L[syst_bin_L]->GetBinContent(eta_bin,pt_bin);
	float eff_T = h_eff_T[syst_bin_CT]->GetBinContent(eta_bin,pt_bin);


        float frac_C = 0;
        float frac_L = 0;
        float frac_T = 0;

        if(mt==0)
        {
            /// 0 lepton channel : take only the nJets parameterisation
            frac_C = h_frac_C_eta_pt[nJets_bin]->GetBinContent(eta_bin,pt_bin);
            frac_L = h_frac_L_eta_pt[nJets_bin]->GetBinContent(eta_bin,pt_bin);
            frac_T = h_frac_T_eta_pt[nJets_bin]->GetBinContent(eta_bin,pt_bin);

            if(syst==7) // MC stat uncertainty
            {
                float frac_C_MCstat = h_frac_C_eta_pt_MCstat[nJets_bin]->GetBinContent(eta_bin,pt_bin);
                float frac_L_MCstat = h_frac_L_eta_pt_MCstat[nJets_bin]->GetBinContent(eta_bin,pt_bin);
                float frac_T_MCstat = h_frac_T_eta_pt_MCstat[nJets_bin]->GetBinContent(eta_bin,pt_bin);

                frac_C = sqrt(pow(frac_C,2) + pow(frac_C_MCstat,2));
                frac_L = sqrt(pow(frac_L,2) + pow(frac_L_MCstat,2));
                frac_T = sqrt(pow(frac_T,2) + pow(frac_T_MCstat,2));
            }
        }
        else
        {
            /// 1 lepton channel : take nJets parameterisation weighted by the mT dependence (mT < 80 GeV as reference)

            float weight_mT_C = h_frac_C_mT->GetBinContent(mT_bin)/h_frac_C_mT->GetBinContent(1);
            float weight_mT_L = h_frac_L_mT->GetBinContent(mT_bin)/h_frac_L_mT->GetBinContent(1);
            float weight_mT_T = h_frac_T_mT->GetBinContent(mT_bin)/h_frac_T_mT->GetBinContent(1);

            frac_C = h_frac_C_eta_pt[nJets_bin]->GetBinContent(eta_bin,pt_bin);
            frac_L = h_frac_L_eta_pt[nJets_bin]->GetBinContent(eta_bin,pt_bin);
            frac_T = h_frac_T_eta_pt[nJets_bin]->GetBinContent(eta_bin,pt_bin);

            if(syst==7) // MC stat uncertainty
            {
                float frac_C_MCstat = h_frac_C_eta_pt_MCstat[nJets_bin]->GetBinContent(eta_bin,pt_bin);
                float frac_L_MCstat = h_frac_L_eta_pt_MCstat[nJets_bin]->GetBinContent(eta_bin,pt_bin);
                float frac_T_MCstat = h_frac_T_eta_pt_MCstat[nJets_bin]->GetBinContent(eta_bin,pt_bin);

                frac_C = sqrt(pow(frac_C,2) + pow(frac_C_MCstat,2));
                frac_L = sqrt(pow(frac_L,2) + pow(frac_L_MCstat,2));
                frac_T = sqrt(pow(frac_T,2) + pow(frac_T_MCstat,2));
            }

            frac_C *= weight_mT_C;
            frac_L *= weight_mT_L;
            frac_T *= weight_mT_T;
        }

        eff_fake = frac_C*eff_C + frac_L*eff_L + frac_T*eff_T;
    }
	*/
    return eff_fake;
}


std::vector<double>  MatrixMethod_bjets::GetWeight(std::vector<float> v_pt, std::vector<float> v_eta, std::vector<bool> v_isb,  std::vector<float> v_Beff, std::vector<float> v_Beff_down, std::vector<float> v_Beff_up, int nJets, float mt, float pt_cut)
{
    std::vector<double> EventWeight(9,0.);

    unsigned int nb_jets =  0;

    for (unsigned int ijet=0; ijet < v_pt.size() ; ijet ++)
      {
	float pt = v_pt.at(ijet);
	if(pt<pt_cut) continue;
	nb_jets++;
      }

    if(nb_jets<4) return EventWeight;
    if(nb_jets>11)  nb_jets=11;  /// I didn't manage to invert a matrix with size > 2048 x 2048 on my local cluster, but please try

    /// run over all systematic uncertainties
    //for(unsigned int syst=0;syst<9;syst++)
    for(unsigned int syst=0;syst<1;syst++)
    {

        const unsigned int size_matrice = pow(2,nb_jets);

        /// Vector of real/fake b-tagging efficiency for all selected jets
        std::vector<float> eff_real_jet(nb_jets);
        std::vector<float> eff_fake_jet(nb_jets);

        /// Vectors needed to build the matrix
        std::vector<int> isTL(size_matrice);
        std::vector<float> nb_jets_truth(size_matrice);

        for(unsigned int i=0;i<size_matrice;i++)
        {
            isTL[i] = 1;
            nb_jets_truth[i] = 0;
        }

        /// Fill b-tagging efficiency and mistag rate
        for (unsigned int ijet=0; ijet < nb_jets ; ijet ++)
        {

            float eta = v_eta.at(ijet);
            float pt = v_pt.at(ijet);

            eff_real_jet[ijet] = v_Beff.at(ijet);
            /// Up and down variation of the b-tagging uncertainty for syst = 1 or syst = 4
            if(syst==1)  eff_real_jet[ijet] = v_Beff_down.at(ijet);
            else if(syst==4)  eff_real_jet[ijet] = v_Beff_up.at(ijet);

            eff_fake_jet[ijet] = this->GetFakeEfficiency(eta,pt,nJets,mt,syst);

	    // std::cout << pt_cut << " nb_jets " << nb_jets << " pt = " << pt << " eta = " << eta << " fake = " <<  eff_fake_jet[ijet] << " real = " <<   eff_real_jet[ijet] << std::endl;
	}

        /// Define the matrix
        TMatrixD matrice(size_matrice,size_matrice);

        /// Define the 2 matrix needed to build the full matrix
        std::vector<std::vector<char>> matrice_TL(size_matrice, std::vector<char>(nb_jets)); /// Loose / Tight matrix
        std::vector<std::vector<char>> matrice_RF(size_matrice, std::vector<char>(nb_jets)); /// Real / Fake matrix


        /// Build these 2 matrix
        for(unsigned int j=0;j<nb_jets;j++)
        {
            for(unsigned int i=0;i<size_matrice;i++)
            {
                bool filled = false;
                int k=1;
                while(!filled)
                {
                    if(i<k*size_matrice/pow(2,1+j))
                    {
                        if(k%2==1)
                        {
                            matrice_TL[i][j] = 'T';
                            matrice_RF[i][j] = 'R';
                        }
                        else{
                            matrice_TL[i][j] = 'L';
                            matrice_RF[i][j] = 'F';
                        }
                        filled = true;
                    }
                    k++;
                }
            }
        }

        /// Fill the matrix of efficiency for each jet

        for(unsigned int ijet=0;ijet<nb_jets;ijet++)
        {
            TMatrixD matrice_eff(size_matrice,size_matrice);

            for(unsigned int i=0;i<size_matrice;i++)
            {
                for(unsigned int j=0;j<size_matrice;j++)
                {
                    /// Tight and Real -> real b-tagging efficiency
                    if((matrice_TL[i][ijet]=='T') && (matrice_RF[j][ijet]=='R'))  matrice_eff[i][j] = eff_real_jet[ijet];
                    /// Tight and Fake -> mistag efficiency
                    else if((matrice_TL[i][ijet]=='T') && (matrice_RF[j][ijet]=='F'))  matrice_eff[i][j] = eff_fake_jet[ijet];
                    /// Loose and Real -> 1 - real b-tagging efficiency
                    else if((matrice_TL[i][ijet]=='L') && (matrice_RF[j][ijet]=='R'))  matrice_eff[i][j] = 1. - eff_real_jet[ijet];
                    /// Loose and Fake -> 1 - mistag efficiency
                    else if((matrice_TL[i][ijet]=='L') && (matrice_RF[j][ijet]=='F'))  matrice_eff[i][j] = 1. - eff_fake_jet[ijet];

                    /// Intialize with the first jet of each matrix element
                    if(ijet==0)   matrice[i][j] = matrice_eff[i][j];
                    /// Then multiply by each other jet efficiency
                    else matrice[i][j] *= matrice_eff[i][j];
                }
            }
        }

        /// Invert the matrix
        TMatrixD matrice_inv = matrice.Invert();



        /// Check if the matrix element is relevant for each N(TTT), N(TTL), N(TLT), ...
        for(unsigned int ijet=0;ijet<nb_jets;ijet++)
        {
            for(unsigned int i=0;i<size_matrice;i++)
            {
                /// If expect a tight jet, but !isb -> skip
                if(matrice_TL[i][ijet]=='T' && !v_isb.at(ijet)) isTL[i] *= 0;
                /// If expect a loose jet, but isb -> skip
                else if(matrice_TL[i][ijet]=='L' && v_isb.at(ijet)) isTL[i] *= 0;
            }
        }

        /// Get the number N(RRR), N(RRF), N(RFR), ... etc by skiping each element not relevant for this event
        for(unsigned int i=0;i<size_matrice;i++)
        {
            for(unsigned int j=0;j<size_matrice;j++)
            {
                nb_jets_truth[i] += matrice_inv[i][j]*isTL[j];
            }
        }
        /// Vector containing the factor efficiency to be applied to each    N(RRR), N(RRF), N(RFR), .. to extrapolate to the 3 b-jets region
        std::vector<double> factor_efficiency(size_matrice);
        for(unsigned int i=0;i<size_matrice;i++)  factor_efficiency[i] = 1;

        for(unsigned int i=0;i<size_matrice;i++)
        {
		    int nb_real = 0;
		    int nb_fake = 0;

		    /// Get average b-tagging efficiency and mistag rate to simplify the system for the background with at least 2 fakes
		    double fake_average = 0;

		    for(unsigned int ijet=0;ijet<nb_jets;ijet++)
		    {
                if(matrice_RF[i][ijet]=='F')
                {
                    fake_average += eff_fake_jet[ijet];
                    nb_fake ++;
                }
		    }
		    if(nb_fake>0)  fake_average = fake_average/nb_fake;

            /// Get the fake background up to 5 fake b-jets
		    double factor_1fake = 0.;
		    double factor_2fake = 0;
		    double factor_3fake = 0;
		    double factor_4fake = 0;
		    double factor_5fake = 0;

            if(nb_fake>1) factor_2fake = this->Combination(nb_fake, 2)*pow(fake_average,2)*pow(1-fake_average,nb_fake-2);
            if(nb_fake>2) factor_3fake = this->Combination(nb_fake, 3)*pow(fake_average,3)*pow(1-fake_average,nb_fake-3);
            if(nb_fake>3) factor_4fake = this->Combination(nb_fake, 4)*pow(fake_average,4)*pow(1-fake_average,nb_fake-4);
            if(nb_fake>4) factor_5fake = this->Combination(nb_fake, 5)*pow(fake_average,5)*pow(1-fake_average,nb_fake-5);

            for(unsigned int ijet=0;ijet<nb_jets;ijet++)
            {
                if(matrice_RF[i][ijet]=='R')
                {
                    factor_efficiency[i] *= eff_real_jet[ijet];
                    nb_real++;
                }
            }

            /// Calculate exactly the probability to get 2 real bjets plus exactly 1 fake
            if(nb_real==2)
            {
                for(unsigned int ijet=0;ijet<nb_jets;ijet++)
                {
                    double factor_fake = 0;
                    if(matrice_RF[i][ijet]=='F')
                    {
                        factor_fake = eff_fake_jet[ijet];
                        for(unsigned int ijet2=0;ijet2<nb_jets;ijet2++)
                        {
                            if(ijet2!=ijet && matrice_RF[i][ijet2]=='F') factor_fake *= (1 - eff_fake_jet[ijet2]);
                        }
                        factor_1fake += factor_fake;
                    }
                }
                factor_efficiency[i] *= (factor_1fake + factor_2fake + factor_3fake + factor_4fake + factor_5fake);
            }
            /// >= 2 fake b-jets case
            else if(nb_real==1) factor_efficiency[i] *= (factor_2fake + factor_3fake + factor_4fake + factor_5fake);
            /// >= 3 fake b-jets case
            else if(nb_real==0) factor_efficiency[i] *= (factor_3fake + factor_4fake + factor_5fake);
            /// No fake b-jet case
            else if(nb_real>2)  factor_efficiency[i] = 0;
        }

        /// Final weight computation
        double weight_MM = 0;
        for(unsigned int i=0;i<size_matrice;i++)
        {
            //cout << "i " << i << " nb " <<nb_jets_truth[i] << endl;
            //cout << "factor_efficiency " << factor_efficiency[i] << endl;
            weight_MM  += (factor_efficiency[i]*nb_jets_truth[i]);
            //cout << weight_MM << endl;
        }
        EventWeight.at(syst) = weight_MM;
    }

    return EventWeight;

}
