import ROOT
import logging
import shutil
import os

logging.basicConfig(level=logging.INFO)
from optparse import OptionParser

parser = OptionParser()

# job configuration
parser.add_option("--submitDir", help="dir to store the output", default="submit_dir")
parser.add_option("--inputDS", help="input DS from DQ2", default="none")
parser.add_option("--driver", help="select where to run", choices=("direct", "prooflite", "proof", "grid"), default="direct")
parser.add_option("--nevents", type=int, help="number of events to process for all the datasets")
parser.add_option("--skip-events", type=int, help="skip the first n events")
parser.add_option("-w", "--overwrite", action='store_true', default=True, help="overwrite previous submitDir")
parser.add_option("--debug", help="Debug mode: print extra stuff [0/1]", type="int", default=0)
parser.add_option("--verbose", help="Verbose mode: print even more extra stuff [0/1]", type="int", default=0)
parser.add_option("--process", help="name of the process added to the output grid jobs", default="")
# input configuration
parser.add_option("--dataSource", help="undefined=-1, data=0, FullSim=1, AF-II=2 ", type="int", default=1)
#top correction configuration
parser.add_option("--doTopPtCorrection", help="Applies top-pT correction [0:no, 1:nom, 2:nom+syst]", type="int", default=0)
parser.add_option("--doTtbarHFClassification", help="Computes the ttbar+HF classification [0: no, 1: classifiction only, 2:nominal correction, 3: systematics]", type="int", default=0)
#W/Z+jets correction
parser.add_option("--doWZReweighting", help="Compute the correction for the Sherpa 2.2", type="int", default=0)
# output configuration
parser.add_option("--doPileup", help="pileup flag [0/1]", type="int", default=1)
parser.add_option("--useCorrectedWeights", help="recompute the MC weight for normalisation", type="int", default=1)
parser.add_option("--doSyst", help="Do systemtic variations [0/1]", type="int", default=1)
parser.add_option("--doTruth", help="Truth flag [0/1]", type="int", default=0)
parser.add_option("--doVerboseOutput", help="Verbose HF output flag [0/1]", type="int", default=0)
parser.add_option("--doNTUP", help="Make flat ntuple output [0/1]", type="int", default=1)
parser.add_option("--doNTUPSyst", help="Add systematics in the flat ntuple output [0/1]", type="int", default=1)
parser.add_option("--doxAOD", help="Make xAOD output [0/1]", type="int", default=0)
parser.add_option("--doVRcJets", help="DoVRcJets flag [0/1]", type="int", default=0)
parser.add_option("--doRcJets", help="DoRcJets flag [0/1]", type="int", default=1)
parser.add_option("--MJSumMax", help="Max jets used in MJsum (default=4)", type="int", default=4)
parser.add_option("--akt4JMS", help="flat JMS on akt4 jets to propagate into MJSum (default 10.0)", type="float",default=10.0)
parser.add_option("--doAK10Jets", help="DoAK10Jets flag [0/1]", type="int", default=0)
parser.add_option("--doTRF", help="TRF flag [0/1]", type="int", default=1)
parser.add_option("--doQCD", help="QCD flag [0/1]", type="int", default=0)
parser.add_option("--doBaselineSelection", help="Apply the baseline selection [0/1]", type="int", default=1)
# Matrix method in the ntuples
parser.add_option("--dobTagEff", help="write the b-tagging efficiency", default=False)
parser.add_option("--doMM", help="write the matrix method weights", default=False)
parser.add_option("--doGridNameFix", help="in the case where the samples are user.X:user.X, we need to do a grid name fix", default=False)
parser.add_option('--athenaAccessMode', help='run using athena access mode', default=0)
parser.add_option('--sendFileStatistics', help='turn on sending file info', default=0)
# just for prun
parser.add_option('--useNewCode', help='Run prun with --useNewCode', default=0)
# name of the config file
parser.add_option("--config", help="name of the SUSYTools config file", default="SUSYTools_ttbar.conf")
# test jobs (only running over one file)
parser.add_option('--test', help='Only run over one file', default=0)
parser.add_option('--official', help='Use official production role', default=0)
parser.add_option('--JMSunc', help='JMS uncertainty scheme [Frozen/Extrap]', default="Frozen")

(options, args) = parser.parse_args()

import atexit
@atexit.register
def quite_exit():
    ROOT.gSystem.Exit(0)

logging.info("loading packages")
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")

if options.overwrite:
    import shutil
    shutil.rmtree(options.submitDir, True)

#Set up the job for xAOD access:
ROOT.xAOD.Init().ignore();

# create a new sample handler to describe the data files we use
logging.info("creating new sample handler")
sh_all = ROOT.SH.SampleHandler()

if options.inputDS != "none":
    ROOT.SH.scanRucio (sh_all, options.inputDS);
else :
    search_directories = []
    search_directories = ["/afs/cern.ch/work/l/lvalery/public/ttbar_filter_topq1/"]
    # scan for datasets in the given directories
    for directory in search_directories:
        ROOT.SH.scanDir(sh_all, directory)

# print out the samples we found
logging.info("%d different datasets found scanning all directories", len(sh_all))

# set the name of the tree in our files
sh_all.setMetaString("nc_tree", "CollectionTree")

# this is the basic description of our job
logging.info("creating new job")
job = ROOT.EL.Job()
job.sampleHandler(sh_all)

#Set the xAOD access mode of the job:
if options.athenaAccessMode == '1':
  job.options().setString( ROOT.EL.Job.optXaodAccessMode,ROOT.EL.Job.optXaodAccessMode_athena );

if options.nevents:
    logging.info("processing only %d events", options.nevents)
    job.options().setDouble(ROOT.EL.Job.optMaxEvents, options.nevents)

if options.skip_events:
    logging.info("skipping first %d events", options.skip_events)
    job.options().setDouble(ROOT.EL.Job.optSkipEvents, options.skip_events)

job.options().setDouble (ROOT.EL.Job.optXAODSummaryReport, options.sendFileStatistics);

# add our algorithm to the job
logging.info("creating algorithms")
alg = ROOT.MainAnalysis()
alg.m_doPileup = options.doPileup
alg.m_doSyst = options.doSyst
alg.m_doTruth = options.doTruth
alg.m_doVerboseOutput = options.doVerboseOutput
alg.m_doNTUP = options.doNTUP
alg.m_doNTUPSyst = options.doNTUPSyst
alg.m_doxAOD = options.doxAOD
alg.m_doVRcJets = options.doVRcJets;
alg.m_doRcJets = options.doRcJets;
alg.m_maxjets_mjsum = options.MJSumMax;
alg.m_akt4_JMS = options.akt4JMS;
alg.m_doAK10Jets = options.doAK10Jets;
alg.m_dobTagEff = options.dobTagEff;
alg.m_doMM = options.doMM;
alg.m_dataSource = options.dataSource;
alg.m_debug = options.debug;
alg.m_verbose = options.verbose;
alg.m_doTRF = options.doTRF
alg.m_doBaselineSelection = options.doBaselineSelection
alg.m_doTopPtCorrection = options.doTopPtCorrection
alg.m_doTtbarHFClassification = options.doTtbarHFClassification
alg.m_doWZReweighting = options.doWZReweighting
alg.m_useCorrectedWeights = options.useCorrectedWeights
alg.m_doQCD = options.doQCD
alg.m_config = options.config
alg.m_JMSunc = options.JMSunc

'''
if options.doxAOD:
  minixAOD = ROOT.MinixAOD()
  minixAOD.m_outputFileName = "output_xAOD"
  minixAOD.m_createOutputFile = True
  minixAOD.m_copyFileMetaData = True
  minixAOD.m_copyTriggerInfo = True
  #minixAOD.m_simpleCopyKeys = "EventInfo"
'''
logging.info("adding algorithms")
job.algsAdd(alg)
#if options.doxAOD: job.algsAdd(minixAOD)

# make the driver we want to use:
# this one works by running the algorithm directly
logging.info("creating driver")
driver = None
if (options.driver == "direct"):
    logging.info("running on direct")
    driver = ROOT.EL.DirectDriver()
    logging.info("submit job")
    driver.submit(job, options.submitDir)
elif (options.driver == "prooflite"):
    logging.info("running on prooflite")
    driver = ROOT.EL.ProofDriver()
    logging.info("submit job")
    driver.submit(job, options.submitDir)
elif (options.driver == "proof"):
    logging.info("running on proof")
    driver = ROOT.EL.ProofDriver()
    driver.proofMaster = "swiatlow@atlint03.slac.stanford.edu:21001";
    driver.makeParOptions = "--nobuild";
    driver.proofParm.setString ("PROOF_LookupOpt", "none");
    driver.proofParm.setString ("PROOF_INTWAIT", "30000")
    driver.proofParm.setString ("PROOF_INITCMD",  ROOT.gSystem.GetFromPipe("source $ROOTCOREBIN/../rcSetup.sh -P").Data())
    driver.submit (job, options.submitDir);
elif (options.driver == "grid"):
    logging.info("running on Grid")
    driver = ROOT.EL.PrunDriver()
    templateName = "user.%nickname%."
    if options.official:
        templateName = "group.phys-exotics."
        driver.options().setString(ROOT.EL.Job.optSubmitFlags, "--official");

    tagName = "2.4.28-3-0"
    if options.doGridNameFix:
      driver.options().setString("nc_outputSampleName", templateName+"%in:name[4]%."+options.process+".%in:name[7]%.%in:name[8]%_"+tagName)
    else:
      driver.options().setString("nc_outputSampleName", templateName+"%in:name[2]%."+options.process+".%in:name[5]%.%in:name[6]%_"+tagName)

    if options.test:
        driver.options().setDouble("nc_nFiles", 1)

    driver.options().setDouble("nc_nGBPerJob", 1)

    if options.useNewCode == '1':
        driver.options().setString( "nc_useNewCode", "")

    logging.info("submit job")
    driver.submitOnly(job, options.submitDir)
