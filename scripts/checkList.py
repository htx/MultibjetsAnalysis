import ROOT
import logging
import shutil
import os
import glob
import subprocess
from optparse import OptionParser

parser = OptionParser()
parser.add_option("--listFiles", help="list files to be checked", default="")
parser.add_option("--derivation", help="derivation to consider", default="TOPQ")
(options, args) = parser.parse_args()

##______________________________________________________________________________
##
def checkSample( sample ):
    com = "rucio ls \""+sample+"\" | grep CONTAINER | grep -c "+options.derivation
    proc = subprocess.Popen(com,stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    (out,err) = proc.communicate()
    out = out.replace("\n","")
    nFiles = 0
    if(out!=""):
        nFiles = int(out)
    print sample
    if nFiles>1:
        os.system("echo -e '     \\033[41;1;37m=> has " + `nFiles` + " sample \\033[0m'")
    elif nFiles==0:
        os.system("echo -e '     \\033[41;1;37m=> has 0 sample \\033[0m'")
    else:
        os.system("echo -e '     \\033[42;1;37m=> is OK !\\033[0m'")


#Looks for the text files to loop over
list_files = glob.glob(options.listFiles)

##______________________________________________________________________________
##
for file in list_files:
    f = open(file,"read")
    for line in f:
        line = line.replace("\n","")
        if line.find("#")>-1:
            continue
        checkSample(line)
