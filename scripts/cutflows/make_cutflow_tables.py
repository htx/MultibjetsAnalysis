import glob
import ROOT
import os
import sys
import json
import re

# return an iterable of the selection being added as well as the selection to apply
def make_selection_strings(selections):
  selection_strings = []
  for i in range(len(selections)):
    selection_strings.append((selections[i],'&&'.join(selections[0:i+1])))
  return selection_strings

def get_weighted_counts(tree, selection, event_weight):
  canvas = ROOT.TCanvas('test', 'test', 200, 10, 100, 100)
  tree.Draw('1>>htemp', '({0:s})*({1:s})'.format(selection,event_weight))
  htemp = canvas.GetPrimitive('htemp')
  raw_sum = htemp.GetEntries()
  weight_sum = htemp.Integral()
  del htemp
  canvas.Clear()
  del canvas
  return raw_sum, weight_sum

def get_scaleFactor(weight):
  scaleFactor = 1.0
  scaleFactor /= weight.get('num events')
  scaleFactor *= weight.get('cross section')
  scaleFactor *= weight.get('filter efficiency')
  scaleFactor *= weight.get('k-factor')
  return scaleFactor

did_regex = re.compile('\.(?:00)?(\d{6})\.')
def get_did(filename):
  global did_regex
  global logger
  m = did_regex.search(filename)
  if m is None:
    logger.warning('Can\'t figure out the DID! Using input filename: {0}'.format(filename))
    return filename.split("/")[-1]
  return m.group(1)

selections = json.load(file('selections.json'))
mappings = json.load(file('mappings.json'))
samples = glob.glob("results/data-output_histfitter/*.root")
weights = json.load(file('weights.json'))

lumi = 14784.5

did_info = {
  "370161": {
    "masses": ["1.8 TeV","200 GeV"],
    "raw": 97000.0,
    "weight": 0.00275737000629,
    "signal": "Gtt"
  },
  "370163": {
    "masses": ["1.8 TeV","600 GeV"],
    "raw": 98000.0,
    "weight": 0.00275675952435,
    "signal": "Gtt"
  },
  "370166": {
    "masses": ["1.8 TeV","1.2 TeV"],
    "raw": 98000.0,
    "weight": 0.00275676138699,
    "signal": "Gtt"
  },
  "370323": {
    "masses": ["1.8 TeV","600 GeV"],
    "raw": 10000.0,
    "weight": 0.00275818281807,
    "signal": "Gbb"
  }
}

dids = [get_did(os.path.basename(sample)) for sample in samples]

weight_lumi_string = '({0:s})*{1:0.4f}'.format( '+'.join('((channel_number=={0:s})*{1:0.12f})'.format(did, get_scaleFactor(weight)) for did, weight in weights.iteritems() if did in dids), lumi )
event_weight = "weight_mc*{0:s}".format(weight_lumi_string)


for sample in samples:
  did = get_did(os.path.basename(sample))
  info = did_info[did]
  f = ROOT.TFile(sample)
  t = f.Get('nominal')

  signal = info['signal']
  nominal_raw = info['raw']
  nominal_weight = info['weight']*lumi

  print("\\begin{table}[h]")
  print("  \\centering")
  print("  \\renewcommand{\\arraystretch}{0.5}")
  print("  \\begin{tabular}{ c c c c }")
  print("    \\toprule")
  print(" "*4+"\\multicolumn{{4}}{{c}}{{Signal: {2:s} ($m_{{\\tilde{{g}}}} = \\mathrm{{{0:s}}}$, $m_{{\\tilde{{\\chi_1}}}} = \\mathrm{{{1:s}}}$)}}\\\\".format(info['masses'][0], info['masses'][1], info['signal']))
  print("{0:^25s} & {1:^20s} & {2:^20s} & {3:^20s}".format("Selection","$N^\mathrm{raw}_\mathrm{events}$","$N^\mathrm{{weight}}_\mathrm{{events}} ({0:0.1f} \mathrm{{fb}}^{{-1}}$)".format(lumi/1000.0), "Efficiency (\%)\\\\"))
  print(" "*4+"\\midrule"*2)
  print(" "*4+"{0:85s}".format("\\multicolumn{4}{c}{Preselection}\\\\"))
  print("    \\midrule")
  print(" "*4+"{0:>25s} & {1:20.2f} & {2:20.2f} & {3:20.2f}\\\\".format("No cut", nominal_raw, nominal_weight, 100.0))
  for name,cut in make_selection_strings(selections['presel']):
    raw, weight = get_weighted_counts(t, cut, event_weight)
    efficiency = (weight/nominal_weight)*100.0
    print(" "*4+"{0:>25s} & {1:20.2f} & {2:20.2f} & {3:20.2f}\\\\".format(mappings.get(name,''), raw, weight, efficiency))

  for channel,details in selections['channels'].iteritems():
    if signal not in channel: continue
    print(" "*4+"\\midrule")
    print(" "*4+"{0:85s}".format("\multicolumn{{4}}{{c}}{{{0:s} Preselection}}\\\\".format(channel.replace('_',' '))))
    print(" "*4+"\\midrule")
    #raw, weight = get_weighted_counts(tc, ['&&'.join(selections['presel'])] + details['presel'], event_weight)
    for name,cut in make_selection_strings(['&&'.join(selections['presel'])] + details['presel']):
      raw, weight = get_weighted_counts(t, cut, event_weight)
      efficiency = (weight/nominal_weight)*100.0
      print(" "*4+"{0:>25s} & {1:20.2f} & {2:20.2f} & {3:20.2f}\\\\".format(mappings.get(name,'Preselection'), raw, weight, efficiency))
    for region,region_cut in details['regions'].iteritems():
      print(" "*4+"\\midrule")
      print(" "*4+"{0:85s}".format("\multicolumn{{4}}{{c}}{{{0:s} {1:s}}}\\\\".format(channel.replace('_',' '), region)))
      for name,cut in make_selection_strings(['&&'.join(selections['presel']+details['presel'])] + region_cut):
        raw, weight = get_weighted_counts(t, cut, event_weight)
        efficiency = (weight/nominal_weight)*100.0
        print(" "*4+"{0:>25s} & {1:20.2f} & {2:20.2f} & {3:20.2f}\\\\".format(mappings.get(name,'{0:s} Preselection'.format(signal)), raw, weight, efficiency))
  print(" "*4+"\\bottomrule")

  print("  \end{tabular}")
  print("  \caption{{The cutflow for the {1:s} channel with gluino mass {2:s} and neutralino mass {3:s} using the signal region definitions. \label{{tab:cutflow_{0:s}}}}}".format(did, signal, info['masses'][0], info['masses'][1]))
  print("\end{table}")
  print("\n"*3)
