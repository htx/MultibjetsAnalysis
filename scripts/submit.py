#!/usr/bin/env python
import os
import sys
import argparse
parser = argparse.ArgumentParser(description='Submit MBJ jobs to Grid.')
parser.add_argument('configs', metavar='process.list', type=str, nargs='+', help='Pass in the processes you want to run.')
parser.add_argument('-n', '--dry-run', dest='dry_run', action='store_true', help='Do a dry-run, do not execute the submission')

args = parser.parse_args()

for config_file in args.configs:
  run_configs = ''
  process = os.path.splitext(os.path.basename(config_file))[0]
  try:
    # Loop over datasets
    for line in open(config_file):
      if not line.strip(): continue
      line = line.strip()
      if line.startswith("# config:"):
        run_configs = line.replace("# config:", "").strip()
        continue
      if line.startswith("#"): continue
      dataset = line
      cmd = 'python MultibjetsAnalysis/scripts/Run.py {0:s} --process {1:s} --submitDir {1:s} --inputDS "{2:s}" --driver grid'.format(run_configs, process, dataset)
      print cmd
      if not args.dry_run:
        os.system(cmd)
        clean =  'rm -r %s' % (process)
        os.system(clean)
  except:
    print "Unexpected error for {0:s}:".format(config), sys.exc_info()
